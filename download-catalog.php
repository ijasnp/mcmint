<?php
include_once('templates/header.php');
?>
<div class="GetCatalougBox fixedPaddingTop ">
	<div class="DownloadCatalogBox">
		<div class="container fixedWidth">
			<div class="row justify-content-center ">
				<div class="col-md-9">
					<div class="HeadingBoxTp11">
						<h3>DOWNLOADS</h3>
						<h2>GET OUR CATALOGUE!</h2>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="catalogLinkBox">
								<ul>
									<li><a href="#">Master Catalog 2018/2019</a></li>
									<li><a href="#">Conference Catalog 2018/2019</a></li>
									<li><a href="#">Party &amp; Wedding Catalog 2018/2019</a></li>
									<li><a href="#">Tablescapes Catalog 2018/2019</a></li>
									<li><a href="#">Exhibition Catalog 2018/2019</a></li>
									<li><a href="#">Prop Catalog 2018/2019</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6">
							<div class="FormBoxTp11">
								<form>
									<input type="text" name="fname" placeholder="Name" />
									<input type="email" name="email" placeholder="Email" />
									<input type="tel" name="mphone" placeholder="Mobile Number" />
									<button class="CatalogBtnTp1">Download All</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="formarea">
			<div class="downloadpopup">
				<h3>DOWNLOADS</h3>
				<h2>GET OUR CATALOGUE!</h2>
			</div>
		</div>
		<div class="row d-flex align-item-center">
			<div class="col-md-6">
				<div class="formarea">
					<div class="downloadpopup">
						<ul>
							<li><a href="#">Master Catalog 2018/2019</a></li>
							<li><a href="#">Conference Catalog 2018/2019</a></li>
							<li><a href="#">Party & Wedding Catalog 2018/2019</a></li>
							<li><a href="#">Tablescapes Catalog 2018/2019</a></li>
							<li><a href="#">Exhibition Catalog 2018/2019</a></li>
							<li><a href="#">Prop Catalog 2018/2019</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="FormBoxTp1">
					<div class="formarea">
						<div class="downloadpopup">
							<form>
								<input type="text" name="fname" placeholder="Name" />
								<input type="email" name="email" placeholder="Email" />
								<input type="tel" name="mphone" placeholder="Mobile Number" />
								<a href="#" class="navLinkTp103">
									download All
								</a>
							</form>
						</div>


					</div>

				</div>
			</div>
		</div> -->
		</div>
	</div>
</div>
<?php
include_once('templates/footer.php');
?>