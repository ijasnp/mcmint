<?php
include_once('templates/header.php');
?>
<div class="contactUsWrpBox fixedPaddingTop">
   <div class="contact-area">
      <div class="container fixedWidth">
         <h2>Contact Us</h2>
         <h4 class="SubTitleTp1"> Nunc vel risus commodo viverra maecenas accumsan. Sem integer vitae justo eget
            magna fermentum iaculis
         </h4>
         <div class="address-area">
            <section class="row p-0 m-0">
               <aside class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 p-0 m-0">
                  <h3>Dubai</h3>
                  <h4>Mint Hire Hatch</h4>
                  <p>Industrial Area 3, Al Quoz<br>
                     Al Asayal Street<br>
                     P.O Box 503005<br>
                     Dubai, United Arab Emirates<br>
                     Phone: <a href="tel:800 MINT (6468)">800 MINT (6468)</a><br>
                     Email: <a href="mailto:info@minthirehatch.com">info@minthirehatch.com</a>
                  </p>
               </aside>
               <aside class="col-12 col-sm-6 col-md-6 col-lg-8 col-xl-8">
                  <h3>Saudi Arabia</h3>
                  <h4>Mint Hire Hatch</h4>
                  <p>Industrial Area Name, Location name<br>
                     Street Name<br>
                     P.O Box No<br>
                     Riyadh, Saudi Arabia<br>
                     Email: <a href="mailto:info@minthirehatch.com">info@minthirehatch.com</a>
                  </p>
               </aside>
            </section>
         </div>
      </div>
   </div>
   <div class="maparea">
      <div class="container fixedWidth">
         <section class="row FixedHeightRow100">
            <aside class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
               <h3 class="m-0 p-0">Reach Us</h3>
               <h4 class="mt-0 mr-0 mb-0 p-0">location Map</h4>
            </aside>
            <aside class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
               <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item">
                     <a class="nav-link active" data-toggle="tab" href="#home">
                        Saudi
                        <span class="ArrowDownIcon"><img src="assets/images/mapDownIcon.png"/></span>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" data-toggle="tab" href="#menu1">
                        Dubai
                        <span class="ArrowDownIcon"><img src="assets/images/mapDownIcon.png"/></span>
                     </a>
                  </li>
               </ul>
            </aside>
         </section>
         <div class="clear"></div>
      </div>
      <!-- Tab panes -->
      
      <div class="clear"></div>
   </div>
   <div class="clear"></div>
   <div class="tab-content">
         <div id="home" class="tab-pane active">
            <a href="https://www.google.com/maps/place/Mint+Event+Rentals+-+HQ/@25.1148032,55.2177428,17z/data=!3m1!4b1!4m5!3m4!1s0x3e5f6bfd5b8b0ffd:0x1d7dc442c2775eb1!8m2!3d25.1147984!4d55.2199315" target="_blank"><img src="assets/images/map1.png" alt="" class="img-fluid" style="width: 100%;"></a>
            <!-- <figure><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d58068.72829095267!2d46.86339416710377!3d24.54444793271586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2fa184b5b3d935%3A0x3e1ad788dc2fddbf!2sNew%20Industrial%20Area%2C%20Riyadh%20Saudi%20Arabia!5e0!3m2!1sen!2s!4v1567929005238!5m2!1sen!2s" width="100%" height="1099" frameborder="0" style="border:0;" allowfullscreen=""></iframe></figure> -->
         </div>
         <div id="menu1" class="tab-pane fade">
         <a href="https://www.google.com/maps/place/Mint+Event+Rentals+-+HQ/@25.1148032,55.2177428,17z/data=!3m1!4b1!4m5!3m4!1s0x3e5f6bfd5b8b0ffd:0x1d7dc442c2775eb1!8m2!3d25.1147984!4d55.2199315" target="_blank"><img src="assets/images/map1.png" alt="" class="img-fluid" style="width: 100%;"></a>
            <!-- <figure><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3612.165658772762!2d55.22479261448303!3d25.130089640587855!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f69628e70cd13%3A0x90c3c3a281979684!2sBrilliant%20Cars!5e0!3m2!1sen!2s!4v1567928791241!5m2!1sen!2s" width="100%" height="1099" frameborder="0" style="border:0;" allowfullscreen=""></iframe></figure> -->
         </div>
      </div>
</div>
<?php
include_once('templates/footer.php');
?>