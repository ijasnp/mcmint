<?php
include_once('templates/header.php');
?>
<div class="FaqBoxTp1">
	<div class="content-area">
		<div class="container fixedWidth">
			<h1>Frequently asked questions</h1>
			<h3>!Got a question you're dying to know the answer to? Check out the below</h3>

		</div>

	</div>
	<div class="bg-gray" id="accordion-style-1">

		<section class="">
			<div class="row">

				<div class="col-12 mx-auto">
					<div class="accordion" id="accordionExample">
						<div class="card m-0 p-0">
							<div class="card-header" id="headingOne">
								<div class="container fixedWidth">
									<h5 class="mb-0">
										<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											Lorem ipsum dolor sit amet, consectetur ?
											<div class="fa closeBtn1 main">
												<div class="mdiv">
													<div class="md"></div>
												</div>
											</div>
											<img class="faqArrowIcon" src="assets/images/faqArrow.png" alt="">
										</button>
									</h5>
								</div>
							</div>
							<div id="collapseOne" class="collapse show fade" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="container fixedWidth">
									<div class="card-body">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.<a href="https://www.fiverr.com/sunlimetech/fix-your-bootstrap-html-and-css3-issues" class="ml-3" target="_blank"><strong>View More designs <i class="fa fa-angle-double-right"></i></strong></a>
									</div>
								</div>
							</div>
						</div>
						<div class="card m-0 p-0">
							<div class="card-header" id="headingOne">
								<div class="container fixedWidth">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
											Lorem ipsum dolor sit amet, consectetur ?
											<div class="fa closeBtn1 main">
												<div class="mdiv">
													<div class="md"></div>
												</div>
											</div>
											<img class="faqArrowIcon" src="assets/images/faqArrow.png" alt="">
										</button>
									</h5>
								</div>
							</div>
							<div id="collapseTwo" class="collapse fade" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="container fixedWidth">
									<div class="card-body">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.<a href="https://www.fiverr.com/sunlimetech/fix-your-bootstrap-html-and-css3-issues" class="ml-3" target="_blank"><strong>View More designs <i class="fa fa-angle-double-right"></i></strong></a>
									</div>
								</div>
							</div>
						</div>
						<div class="card m-0 p-0">
							<div class="card-header" id="headingOne">
								<div class="container fixedWidth">
									<h5 class="mb-0">
										<button class="btn btn-link collapsed btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
											Lorem ipsum dolor sit amet, consectetur ?
											<div class="fa closeBtn1 main">
												<div class="mdiv">
													<div class="md"></div>
												</div>
											</div>
											<img class="faqArrowIcon" src="assets/images/faqArrow.png" alt="">
										</button>
									</h5>
								</div>
							</div>
							<div id="collapseThree" class="collapse fade" aria-labelledby="headingOne" data-parent="#accordionExample">
								<div class="container fixedWidth">
									<div class="card-body">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.<a href="https://www.fiverr.com/sunlimetech/fix-your-bootstrap-html-and-css3-issues" class="ml-3" target="_blank"><strong>View More designs <i class="fa fa-angle-double-right"></i></strong></a>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</section>

	</div>
</div>
<?php
include_once('templates/footer.php');
?>
<!-- .// Accordion -->