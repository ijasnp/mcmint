<?php
include_once('templates/header.php');
?>
<div class="GalleryPage">
	<div class="slider-area">
		<div class="slider-active owl-dot-style owl-carousel">
			<div class="single-slider bg-img d-flex align-items-center justify-content-center" style="background-image:url(assets/images/header-img01.jpg);">
				<div class="slider-content pt-100" data-aos="fade-down" data-aos-duration="500">
					<div class="slider-content-wrap slider-animated-1">
						<h2 class="animated">#minteventrental</h2>
					</div>
				</div>
			</div>
			<div class="single-slider bg-img d-flex align-items-center justify-content-center" style="background-image:url(assets/images/header-img02.jpg);">
				<div class="slider-content pt-100">
					<div class="slider-content-wrap slider-animated-1">
						<h2 class="animated">#minteventrental</h2>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="content-area">
		<div class="container">
			<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
		</div>

	</div>
	<div class="gallery-area">
		<div class="container-fluid">

			<section class="row">
				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img01.jpg" data-fancybox-group="gallery" title="Title Two"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img01.jpg" alt="" /></a>
						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>

				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img02.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img02.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>

				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img03.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img03.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>
				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img03.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img03.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>

				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img04.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img04.jpg" alt="hi dear" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>
				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img05.jpg" data-fancybox-group="gallery" title="Title Two"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img05.jpg" alt="" /></a>
						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>

				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img06.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img06.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>

				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img07.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img07.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>
				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img08.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img08.jpg" alt="hi dear" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>
				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img09.jpg" data-fancybox-group="gallery" title="Title Two"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img09.jpg" alt="" /></a>
						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>
				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img03.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img03.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>
				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img03.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img03.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>

				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img10.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img10.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>

				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img11.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img11.jpg" alt="" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>
				<aside class="col-12 col-sm-6 col-md-6 col-lg-3 col-xl-3" data-aos="fade-up" data-aos-duration="1100">
					<figure><a class="fancybox" href="assets/images/big/gb-img12.jpg" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet"><img src="assets/images/placeholder.gif" class="lazy" data-src="assets/images/gs-img12.jpg" alt="hi dear" /></a>

						<div class="newTitle" style="display: none;">
							<h1>Image Name</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
						</div>
					</figure>
				</aside>

			</section>
			<div class="ShowMoreImageBox text-center m-auto p-5">
				<a href="#" class="ShowMoreImageLink text-uppercase">show more images</a>
			</div>

		</div>

	</div>
</div>
<?php
include_once('templates/footer.php');
?>