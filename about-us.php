<?php
include_once('templates/header.php');
?>
<div class="AboutUsPage">
   <div class="slider-area" style="display: none;">
      <div class="slider-active owl-dot-style owl-carousel">
         <div class="single-slider bg-img d-flex align-items-center justify-content-center" style="background-image:url(assets/images/header-img01.jpg);">
            <div class="slider-content pt-100" data-aos="fade-down" data-aos-duration="500">
               <div class="slider-content-wrap slider-animated-1">
               <h2 class="animated">#ABOUT US</h2>
               </div>
            </div>
         </div>
         <div class="single-slider bg-img d-flex align-items-center justify-content-center" style="background-image:url(assets/images/header-img02.jpg);">
            <div class="slider-content pt-100">
               <div class="slider-content-wrap slider-animated-1">
                  <h2 class="animated">#ABOUT US</h2>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="history-area" data-aos="fade-up" data-aos-duration="600">
      <div class="container fixedWidth">
         <section class="row">
            <aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
               <h2>10 years<br>OF JOURNEY</h2>
               <h3>Our History</h3>
            </aside>
            <aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
               <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
               <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </aside>
         </section>
      </div>
   </div>
   <div class="qc-area" data-aos="fade-up" data-aos-duration="600">
      <div class="container-fluid">
         <section class="row">
            <aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 nogrid-left">
               <figure><img src="assets/images/qc-bg.jpg" alt="About Us" /></figure>
            </aside>
            <aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-5">
               <h2>QUALITY<br>CONTROL</h2>
               <h3>Lorem ipsum dolor sit amet<br>
                  consectetur
               </h3>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
               <p class="m-0 p-0">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </aside>
         </section>
      </div>
   </div>
   <div class="deliverytracking-area" data-aos="fade-up" data-aos-duration="600">
      <div class="deliveryl">Delivery</div>
      <!-- <div class="tranckingc">Tracking</div> -->
      <div class="DeliveryTrackinBoxTp1">
      <div class="container">
         <section class="row">
            <aside class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
               <h3>ON TIME, EVERY TIME</h3>
               <h2>DELIVERY <br>TRACKING</h2>
               <ul>
                  <li>View all your orders online</li>
                  <li>Track your deliveries in real time</li>
                  <li>Receive prompt alerts</li>
               </ul>
               <p>*for corporate customers only</p>
               <a href="#" class="partner-btn mb-5">Partner with us</a>
            </aside>
            <aside class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 p-0">
               <figure class="m-0"><img src="assets/images/aboutUsTracking.png" alt="phone" /></figure>
            </aside>
         </section>
      </div>
      </div>
   </div>
   <div class="aboutdetail-area" data-aos="fade-up" data-aos-duration="600">
      <div class="container-fluid">
         <section class="row">
            <aside class="col-12 col-sm-12 col-md-12 col-lg-12  col-xl-5 offset-xl-1">
               <h2>Lorem ipsum dolor sit</h2>
               <h3>Lorem ipsum dolor sit amet
                  consectetur
               </h3>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
               <p class="p-0">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </aside>
            <aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 nogrid-right">
               <figure class="imgbox"><img src="assets/images/about-details.png" alt="aaa" /></figure>
            </aside>
         </section>
      </div>
   </div>
   <div class="aboutteam-area" data-aos="fade-up" data-aos-duration="600">
      <h2>OUR TEAM</h2>
      <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
      <figure><img src="assets/images/about-team.jpg" alt="Our Team" /></figure>
      <a href="#" class="timgbtn">
         <div class="inner"></div>
      </a>
   </div>
   <div class="wharehose-area" data-aos="fade-up" data-aos-duration="600">
      <div class="container-fluid">
         <section class="row">
            <aside class="col-12 col-sm-12 col-md-12 col-lg-5 offset-lg-1 col-xl-5 offset-xl-1">
               <h2>WaREHOUSE<br>
                  dolor sit
               </h2>
               <h3>Lorem ipsum dolor sit
                  amet consectetur
               </h3>
               <p class="p-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            </aside>
            <aside class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 nogrid-right">
               <figure><img src="assets/images/wharehouse-img.jpg" alt="Wharehouse" /></figure>
            </aside>
         </section>
      </div>
   </div>
</div>
<?php
include_once('templates/footer.php');
?>

