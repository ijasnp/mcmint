<?php
include_once('templates/header.php');
?>
<div class="CartBoxTp1 FixedToNavBottom">
	<div class="content-area-cart">
		<div class="NoProductBox" data-aos="fade-up" data-aos-duration="600" style="display: none;">
			<div class="text-center">
				<div class="NoProduCtImage">
					<img src="assets/images/noproductfound.png" alt="">
				</div>
				<div class="NoProductBoxTxt1"><span>Your</span>Cart Is Empty</div>
			</div>
		</div>
		<div class="container fixedWidth">
			<section class="row">
				<aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8">
					<h1 data-aos="fade-up">Hey&nbsp;<span>there,</span></h1>
					<h3 data-aos="fade-up" data-aos-duration="600">Here are the items you added to your cart</h3>
					<div class="ProDucItemBoxWrp">
						<div class="productbox clearfix" data-aos="fade-up" data-aos-duration="600">
							<div class="imgBoxTp11">
								<figure class="imgbox"><img src="assets/images/product01.png" alt="Product 01" /></figure>
								<span class="ReMoveItemButton MobOnly">Remove Item</span>
							</div>
							<div class="pl-4 pr-4 contentBoxTp11">
								<h2>RENEE DINING CHAIR - SALMON PINK + PINK SEATPAD</h2>
								<div class="colorbox">
									<div class="newtag"><img src="assets/images/newtag.png" alt=""></div>
									Various Colors
								</div>
							</div>
							<div class="qtyinde CountBoxTp1">
								<div class="btn-minus DesktopOnly"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
								<input class="DeskTopInputTp1" type="number" value="1" />
								<div class="btn-plus DesktopOnly"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
								<div class="InputBoxTp11 MobOnly">
									<button type="button" id="add" class="add">
									<i class="fa fa-angle-up" aria-hidden="true"></i>
									</button>
									<input class="text-center" type="number" id="1" value="1" min="1" max="3" />
									<button type="button" id="sub" class="sub">
										<i class="fa fa-angle-down" aria-hidden="true"></i>
									</button>
								</div>
							</div>
							<div class="closedbtn DesktopOnly">
								<div class="closeBtn1">
									<div class="mdiv">
										<div class="md"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="productbox clearfix" data-aos="fade-up" data-aos-duration="600">
							<div class="imgBoxTp11">
								<figure class="imgbox"><img src="assets/images/product01.png" alt="Product 01" /></figure>
								<span class="ReMoveItemButton MobOnly">Remove Item</span>
							</div>
							<div class="pl-4 pr-4 contentBoxTp11">
								<h2>RENEE DINING CHAIR - SALMON PINK + PINK SEATPAD</h2>
								<div class="colorbox">
									<div class="newtag"><img src="assets/images/newtag.png" alt=""></div>
									Various Colors
								</div>
							</div>
							<div class="qtyinde CountBoxTp1">
								<div class="btn-minus DesktopOnly"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
								<input class="DeskTopInputTp1" type="number" value="1" />
								<div class="btn-plus DesktopOnly"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
								<div class="InputBoxTp11 MobOnly">
									<button type="button" id="add" class="add">
									<i class="fa fa-angle-up" aria-hidden="true"></i>
									</button>
									<input class="text-center" type="number" id="1" value="1" min="1" max="3" />
									<button type="button" id="sub" class="sub">
										<i class="fa fa-angle-down" aria-hidden="true"></i>
									</button>
								</div>
							</div>
							<div class="closedbtn DesktopOnly">
								<div class="closeBtn1">
									<div class="mdiv">
										<div class="md"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="productbox clearfix" data-aos="fade-up" data-aos-duration="600">
							<div class="imgBoxTp11">
								<figure class="imgbox"><img src="assets/images/product01.png" alt="Product 01" /></figure>
								<span class="ReMoveItemButton MobOnly">Remove Item</span>
							</div>
							<div class="pl-4 pr-4 contentBoxTp11">
								<h2>RENEE DINING CHAIR - SALMON PINK + PINK SEATPAD</h2>
								<div class="colorbox">
									<div class="newtag"><img src="assets/images/newtag.png" alt=""></div>
									Various Colors
								</div>
							</div>
							<div class="qtyinde CountBoxTp1">
								<div class="btn-minus DesktopOnly"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
								<input class="DeskTopInputTp1" type="number" value="1" />
								<div class="btn-plus DesktopOnly"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
								<div class="InputBoxTp11 MobOnly">
									<button type="button" id="add" class="add">
									<i class="fa fa-angle-up" aria-hidden="true"></i>
									</button>
									<input class="text-center" type="number" id="1" value="1" min="1" max="3" />
									<button type="button" id="sub" class="sub">
										<i class="fa fa-angle-down" aria-hidden="true"></i>
									</button>
								</div>
							</div>
							<div class="closedbtn DesktopOnly">
								<div class="closeBtn1">
									<div class="mdiv">
										<div class="md"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="CartBtnBoxTp1 mt-4" data-aos="fade-up" data-aos-duration="600">
						<div class="d-flex align-items-center justify-content-between">
							<a href="#" class="CartLinkTp1">
								<span class="d-flex align-items-center justify-content-start">
									<span class="mr-3"><i class="fa fa-angle-double-left mr-1" aria-hidden="true"></i></span>
									<span>Continue Shopping</span>

								</span>
							</a>
							<a href="#" class="CartLinkTp2">
								<span class="d-flex align-items-center justify-content-start text-uppercase">
									Remove All Cart Items
								</span>
							</a>
						</div>
					</div>
				</aside>
				<aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4" data-aos="fade-up" data-aos-duration="600">
					<div class="cart-right-detailbox clearfix">
						<div class="toptag">
							<div class="inner"></div>
						</div>
						<div class="detailinner clearfix">
							<h3>Hire Details</h3>

							<div class="txtbox">
								<div class="iconbox"><img src="assets/images/calender-icon.png" alt="" /></div>
								<!-- <input id="dom-id"  size="60" value="2015/08/09"> -->
								<input type="text" name="reservation" id="reservation" value="03/18/2013 - 03/23/2013" />
							</div>
							<div class="radioarea clearfix">
								<div>
									<label class="radio">
										<input type="radio" name="r" value="1" checked>
										<span>Delivery</span>
									</label>
									<label class="radio">
										<input type="radio" name="r" value="2">
										<span>Pick Up</span>
									</label>
								</div>
							</div>
							<div class="txtbox">
								<div class="iconbox"><img src="assets/images/location-icon.png" alt="" /></div>
								<input type="text" name="location" id="location" onfocus="this.value=''" onblur="this.value='Delivery Location'" value="Delivery Location" />
							</div>
						</div>
						<div class="linebox"></div>
						<div class="bottomdetail">
							<section class="row">
								<aside class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
									 <span style="font-family: 'airbnb_cereal_appmedium';">Hire Dates</span>
									
								</aside>
								<aside class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-right">
									1 Day
								</aside>
							</section>
							<section class="row">
								<aside class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
									<span style="font-family: 'airbnb_cereal_appmedium';">Hire Items</span>
									
								</aside>
								<aside class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-right">
									42 Items
								</aside>
							</section>
							<a href="#" data-toggle="modal" data-target="#CartModal" class="continue-btn text-uppercase text-center transitionBox">Continue</a>
						</div>
					</div>
				</aside>
			</section>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="CartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered CartModalBox" role="document">
		<div class="modal-content">
			<div class="modal-body p-0">
				<div class="formarea">
					<div class="chatbot-area">
						<div class="closeBtn1" data-dismiss="modal" aria-label="Close">
							<div class="mdiv">
								<div class="md"></div>
							</div>
						</div>
						<div class="chat-inner">
							<div class="FigureBoxTp1"><img src="assets/images/chat-img01.png" alt="User Image" /></div>
							<h2 data-aos="fade-up" data-aos-duration="600"><span>Hey</span> Beverly</h2>
							<h3 data-aos="fade-up" data-aos-duration="600">No stress, this will only take 30 seconds</h3>
							<div class="chatbox-inner">
								<div class="p-4">
									<div class="BXChatTp1">
										<div class="chatleft clearfix">
											<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>
											<div class="ctextbox">
												What is your name ?
											</div>
										</div>
										
										<div class="chatright clearfix TypingBox" style="display: none">
											<div class="ctextbox">
												Typing..
											</div>
										</div>
									</div>
									<div class="BXChatTp1" style="display: none">
										<div class="chatleft clearfix">
											<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>
											<div class="ctextbox">
												What is your email address ?
											</div>
										</div>
										<div class="chatright clearfix">
											<div class="ctextbox">
												beverly@gameofthrones.com
											</div>
										</div>
									</div>
									<div class="BXChatTp1" style="display: none">
										<div class="chatleft clearfix">
											<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>
											<div class="ctextbox">
												What date would you like to have the furniture delivered on?
												<input style="display: none;" type="text" placeholder="Pick a date from here.." name="reservation1" id="reservation1" value="PICK A DATE">
												<span id="PickDateBox" class="PickDateBox  DesktopOnly">
													<img src="assets/images/pickadate.png" alt="">
												</span>
											</div>
										</div>
										<div class="chatright clearfix">
											<div class="ctextbox">
												03/18/2013 - 03/23/2013
											</div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="bchatboxx">
									<div class="InputBoxTp1">
										<input class="ChatInputBox" type="text" name="chatbox" placeholder="Type your answer here..." />
										<button class="SubmitBtn1" type="submit"><img src="assets/images/chat-icon-send.png" alt=""></button>
										<script>
											
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once('templates/footer.php');
?>