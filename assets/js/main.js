

$(document).ready(function() {
    $(".MobleSideMenu").on("click", function(e) {

        $(".MobileSideNav").addClass("NavTriggered")
    })

    $(".MobileSearchMenu").on("click", function(e) {
        $(".MobileSearchBox").addClass("NavTriggered")
    })

    $(".CloseSideNav").on("click", function(e) {

        $(".MobileSideNav").removeClass("NavTriggered")
    })
    $(".CloseSearch").on("click", function(e) {

        $(".MobileSearchBox").removeClass("NavTriggered")
    })
    checkforfooterlinks();
    function checkforfooterlinks(){
        if($(window).width()<768){
            $(".MobileLinkTp101").click(function(){
                $(this).next(".MobileBoxTp101").slideToggle();
                $(this).children(".fa-plus").toggle();
                $(this).children(".fa-minus").toggle();
            })
        }
    }
    
});
(function($) {
    'use strict';

    /*--
    Menu Stick
    -----------------------------------*/
    var header = $('.transparent-bar');
    var win = $(window);
    
    win.on('scroll', function() {
        var scroll = win.scrollTop();
        if (scroll < 200) {
            header.removeClass('stick');
        } else {
            header.addClass('stick');
        }
    });

    $( "#ProductDetailModal" ).on('shown.bs.modal', function(){
        $("#exzoom").exzoom({
            "autoPlay": false,
            "navWidth":110,
            "navHeight": 110,
            "navItemNum": 5
        });
        setTimeout(function(){$("#exzoom").removeClass("OpacityMinus"); }, 1000);
    });

    $(".search-wrap button").on("click", function(e) {
        e.preventDefault();
        $(this).parent().find('.search').slideToggle('medium');
    });
   
    
    /* Slider active */
    
    
    
    
    /* Best selling active */
    $('.product-slider-active').owlCarousel({
        loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 5000,
        navText: ['<i class="ion-ios-arrow-back"></i>', '<i class="ion-ios-arrow-forward"></i>'],
        item: 3,
        margin: 30,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 2
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            },
            1200: {
                items: 3
            }
        }
    })
    
    
    $('.grid').imagesLoaded(function() {
        // filter items on button click
        $('.gallery-menu').on('click', 'button', function() {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({
                filter: filterValue
            });
        });
        // init Isotope
        var $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            percentPosition: true,
            masonry: {
                // use outer width of grid-sizer for columnWidth
                columnWidth: '.grid-item',
            }
        });
    });
    
    
    $('.gallery-menu button').on('click', function(event) {
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
        event.preventDefault();
    });
    
    
    /*--
    Magnific Popup
    ------------------------*/
    $('.img-popup').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });
    
    
    /*------ Wow Active ----*/
    new WOW().init();
    
    
    /*--------------------------
        ScrollUp
    ---------------------------- */
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-double-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });
    
    
    /*--
       Testimonial Slick Carousel
       -----------------------------------*/
    $('.testimonial-text-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        draggable: false,
        fade: true,
        asNavFor: '.slider-nav',
    });
    
    
    /*--
        Testimonial Slick Carousel as Nav
    -----------------------------------*/
    $('.testimonial-image-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.testimonial-text-slider',
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '0px',
        responsive: [{
                breakpoint: 767,
                settings: {
                    dots: false,
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 420,
                settings: {
                    autoplay: true,
                    dots: false,
                    slidesToShow: 1,
                    centerMode: false,
                }
            }
        ]
    });
    
    
    /* Brand logo active */
    $('.brand-logo-active').owlCarousel({
        loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 5000,
        item: 5,
        margin: 50,
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 2,
                margin: 30,
            },
            768: {
                items: 4,
                margin: 30,
            },
            992: {
                items: 4,
                margin: 100,
            },
            1200: {
                items: 5
            }
        }
    })
    
    
    /* Brand logo active */
    $('.blog-active').owlCarousel({
        loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 5000,
        item: 3,
        margin: 30,
        responsive: {
            0: {
                items: 1,
            },
            480: {
                items: 2,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 3
            }
        }
    })
    
    
    /*--
    Smooth Scroll
    -----------------------------------*/
    $('.scroll-up').on('click', function(e) {
        e.preventDefault();
        var link = this;
        $.smoothScroll({
            scrollTarget: link.hash
        });
    });
    
    
    /* jQuery MeanMenu */
    $('#mobile-menu-active').meanmenu({
        meanScreenWidth: "991",
        meanMenuContainer: ".mobile-menu-area .mobile-menu",
    });

    /*----------------------------
        youtube video
        ------------------------------ */
    $('.youtube-bg').YTPlayer({
        containment: '.youtube-bg',
        autoPlay: true,
        loop: true,
    });
    
    
    /*---------------------
        Countdown
    --------------------- */
    $('[data-countdown]').each(function() {
        var $this = $(this),
            finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
            $this.html(event.strftime('<span class="cdown day">%-D <p>Days</p></span> <span class="cdown hour">%-H <p>Hour</p></span> <span class="cdown minutes">%M <p>Min</p></span class="cdown second"> <span>%S <p>Sec</p></span>'));
        });
    });
    
    
    /* counterUp */
    $('.count').counterUp({
        delay: 10,
        time: 1000
    });

$(function() {
         
         	//	Basic carousel, no options
         	//$('#foo0').carouFredSel();
         
         	//	Basic carousel + timer, using CSS-transitions
//         	$('#foo1').carouFredSel({
//         		auto: {
//         			pauseOnHover: 'resume',
//         			progress: '#timer1'
//         		}
//         	}, {
//         		transition: true
//         	});
         
         	//	Scrolled by user interaction
//         	$('#foo2').carouFredSel({
//         		auto: false,
//         		prev: '#prev2',
//         		next: '#next2',
//         		pagination: "#pager2",
//         		mousewheel: true,
//         		swipe: {
//         			onMouse: true,
//         			onTouch: true
//         		}
//         	});
         
         	//	Variable number of visible items with variable sizes
         	$('#foo3').carouFredSel({
         		responsive: true,
         		width: '100%',
         		height: '100%',
         		prev: '.prev3',
         		next: '.next3',
         
         
         		auto: false,
         		scroll: 1,
         		items: {
         width: 296,
        height: 680,
         	visible: {
         				min: 1,
         				max: 3,
         
         			}
         		}
         	});
	
	
	//	Variable number of visible items with variable sizes
         	$('#foo33').carouFredSel({
         		responsive: true,
         		width: '100%',
         		height: 'auto',
         		prev: '.prev3',
         		next: '.next3',
         
         
         		auto: false,
         		scroll: 1,
         		items: {
         width: 185,
        //height: 220,
         	visible: {
         				min: 1,
         				max: 5,
         
         			}
         		}
         	});
	
	
	//	Variable number of visible items with variable sizes
         	$('#foo34').carouFredSel({
         		responsive: true,
         		width: '100%',
         		height: 'auto',
         		prev: '.prev03',
         		next: '.next03',
         
         
         		auto: false,
         		scroll: 1,
         		items: {
         width: 185,
       // height: 220,
         	visible: {
         				min: 1,
         				max: 5,
         
         			}
         		}
         	});
	
	
	//	Variable number of visible items with variable sizes
         	
	
	
	
	//	Variable number of visible items with variable sizes
         	
	
	
	
         	
         	//	Variable number of visible items with variable sizes
         	
         
         	//	Responsive layout, resizing the items
//         	$('#foo4').carouFredSel({
//         		responsive: true,
//         		width: '100%',
//         		scroll: 2,
//         		items: {
//         			width: 400,
//         		//	height: '30%',	//	optionally resize item-height
//         			visible: {
//         				min: 2,
//         				max: 6
//         			}
//         		}
//         	});
         
         	//	Fuild layout, centering the items
//         	$('#foo5').carouFredSel({
//         		width: '100%',
//         		scroll: 2
//         	});
         
    });  
	
	

	
	


})(jQuery);



$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox({
				
				afterLoad: function(){
                    this.title = $(this.element).next('.newTitle').html();
                },
                'autoSize': true,
                helpers: {
                title : {
                    type : 'inside'
                }
                }
				
				
			});

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5,
					
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});
 $(".fancyboxx").fancybox({
        'width': '95%',
        'height': 850,
        'transitionIn': 'elastic', // this option is for v1.3.4
        'transitionOut': 'elastic', // this option is for v1.3.4
        // if using v2.x AND set class fancybox.iframe, you may not need this
        'type': 'iframe',
        // if you want your iframe always will be 600x250 regardless the viewport size
        'fitToView' : false  // use autoScale for v1.3.4
    });
	

	
	
	

		});





(function($) {


//$('.mainmenu ul li.has-children>a').click(function() {
//	$(this).parent().siblings().find('ul').slideUp(300);
//	//$(this).parent().siblings().find('ul').slideUp(300);
//	$(this).toggleClass('active').next('ul').stop(true, false, true).slideToggle(300);
//	//$(this).toggleClass('active').next('ul').slideDown(300).next('ul').slideToggle(300);
//	return false;
//
//});	
//  	
//})(jQuery);
	$('#nav ul').hide();
	
//$('.mainmenu').mouseout(function(e){
//	$('.dropdown-menu').siblings().find('ul').slideUp(300);	
//		
//	
//	});		
	
$('#nav  li >a').on("mouseover", function(e){
	//$(this).parent().siblings().find('ul').slideUp(300);
	 $(this).parent().children('ul').stop().slideDown(300);
	//$(this).parent().siblings().find('ul').slideUp(300);
	//$(this).next('ul').toggle(300);
	//$(this).toggleClass('active').next('ul').slideDown(300).next('ul').slideToggle(300);
	// e.stopPropagation();
//    e.preventDefault();
	//return false;
	
	
	

});	
	
 $('#nav li').hover(null, 
      function (e) {
      //hide its submenu
        $(this).children('ul').stop().slideUp(300);
      }
   );
	
	
//$('.mainmenu ul li.has-children>a').on("mouseout", function(e){	
//	$(this).next('ul').slideUp(300);
//	 e.stopPropagation();
//    e.preventDefault();
//	
//});	  	
})(jQuery);	
	




$(document).ready(function() {
		
		$('.sidemenu a').click(function(){
			$('.overlay').css('display','block');
  $('#sideNavigation').css('left',0);
			
  //$("#main").css('margin-left',250);
});
 
$('.closebtn').click(function(){
	$('.overlay').css('display','none');
  $('#sideNavigation').css('left','-100%');
  //$("#main").css('width',0);
});
});		

$(function(e) {
    $('.searchlinkClickHndler').on('click', function(e) {
        // e.preventDefault();
        $(".HeroLogo").toggle();
        $(".searchform").toggle();
        
		$(".SearchFormField").focus();
   
    });
    
});

$(document).ready(function() {
    $('.newArrivalSlider').owlCarousel({
        navText:["<div class='prev'></div>","<div class='next'></div>"],
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:2,
                margin:0,
                center: true,
                loop:true,
                nav:true,
            },
           
            768:{
                items:2,
                margin:0,
                center: true,
                loop:true,
                nav:true
            },
            1000:{
                items:3
            }
        }
    });
    $('.TestimonialCarousel').owlCarousel({
        navText:["<div class='prev'></div>","<div class='next'></div>"],
        margin:0,
        nav:true,
        items:1,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });
    $('.ProductBoxTp1Carousel').owlCarousel({
        navText:["<div class='prev'></div>","<div class='next'></div>"],
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:2,
                margin:10
            },
            600:{
                items:6,
                margin:10
            },
            1000:{
                items:6,
                margin:10
            }
        }
    })
    $('.NvLinkTp1').hover(
    function() {
        $(this).addClass('Active');
        $('.SubNavBox', this).addClass('DropDownActivated');
        $(".HeroLogo").addClass("onShift")
    }, function() {
        $(this).removeClass('Active');
        $('.SubNavBox', this).removeClass('DropDownActivated');
        $(".HeroLogo").removeClass("onShift")
    });
    $('.SubItemTp1Box').hover(
        function() {
            $(this).addClass('Active');
            $('.SubNavBox', this).addClass('DropDownActivated');
            $(".HeroLogo").addClass("onShift")
        }, function() {
            $(this).removeClass('Active');
            $('.SubNavBox', this).removeClass('DropDownActivated');
            $(".HeroLogo").removeClass("onShift")
        });
    var wWidth = $(window).width();

    var navHeight = $(".BxTp1Cnt1").height();
    var subNavHeight1 = $(".SubNavBox").height();

    $(".SubItemBoxTp1").width(wWidth);
    $(".SubItemBoxTp1").css("top",80+subNavHeight1+'px');
});
$(document).ready(function() {
    $('.SubNavBox').hover(
    function() {
        $(this).addClass('Active');
        // var HeightTop = $(this).closest('.SubNavBox').height();
        // var wWidth = $(window).width();
        // $(".SubItemBoxTp1").css("top",HeightTop+80+'px');
        // $(".SubItemBoxTp1").width(wWidth)
    }, function() {
        $(this).removeClass('Active');
    });
});
//-- Click on QUANTITY
$(function(e) {
	
            // $(".btn-minus").on("click",function(){
            //     alert("clicked")
            //     var now = $(this).next(".DeskTopInputTp1").val();
            //     if ($.isNumeric(now)){
            //         if (parseInt(now) -1 > 0){ now--;}
            //         $(this).next(".DeskTopInputTp1").val(now);
            //     }else{
            //         $(this).next(".DeskTopInputTp1").val("1");
            //     }
            // })            
            // $(".btn-plus").on("click",function(){
            //     var now = $(".productbox .qtyinde > input").val();
            //     if ($.isNumeric(now)){
            //         $(".productbox .qtyinde > input").val(parseInt(now)+1);
            //     }else{
            //         $(".productbox .qtyinde > input").val("1");
            //     }
            // })

            
            $('.btn-plus').click(function () {
                if ($(this).prev().val() > 0) {
                    $(this).prev().val(+$(this).prev().val() + 1);
                }
            });
            $('.btn-minus').click(function () {
                    if ($(this).next().val() > 0) {
                        $(this).next().val(+$(this).next().val() - 1);
                    }
            }); 

        
	
			 $(".btn-minus02").on("click",function(){
                var now = $(".productbox > .qtyinde02 > input").val();
                if ($.isNumeric(now)){
                    if (parseInt(now) -1 > 0){ now--;}
                    $(".productbox .qtyinde02 > input").val(now);
                }else{
                    $(".productbox > .qtyinde02 > input").val("1");
                }
            })            
            $(".btn-plus02").on("click",function(){
                var now = $(".productbox .qtyinde02 > input").val();
                if ($.isNumeric(now)){
                    $(".productbox .qtyinde02 > input").val(parseInt(now)+1);
                }else{
                    $(".productbox .qtyinde02 > input").val("1");
                }
            })
	        
			 $(".btn-minuss").on("click",function(){
                var now = $(".details > .qtyindes > input").val();
                if ($.isNumeric(now)){
                    if (parseInt(now) -1 > 0){ now--;}
                    $(".details .qtyindes > input").val(now);
                }else{
                    $(".details > .qtyindes > input").val("1");
                }
            })            
            $(".btn-pluss").on("click",function(){
                var now = $(".details .qtyindes > input").val();
                if ($.isNumeric(now)){
                    $(".details .qtyindes > input").val(parseInt(now)+1);
                }else{
                    $(".details .qtyindes > input").val("1");
                }
            })
	
 
});


/*$(document).ready(function() {
	var s = $(".olddate");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos <=400) {
			s.addClass("olddate02");
		} else {
			s.removeClass("olddate02");	
		}
	});
});
*/






$(window).scroll(function () { 
         
         console.log($(window).scrollTop());
         
         if ($(window).scrollTop() > 1000) {
         
          $('.olddate').addClass('olddate02');
         }
         
         
         if ($(window).scrollTop() < 1000) {
          $('.olddate').removeClass('olddate02');
          
          
         }
         
         });


function unoptimized() {
		var scrollPosition = $(window).scrollTop();

		$('.section').each(function() {
		    var sectionTop = $(this).offset().top;
		    var id = $(this).attr('id');

		    if (scrollPosition >= sectionTop) {
		        $('.olddate').removeClass('olddate02');
		        $('.olddate').addClass('olddate02');
		    }
		});
	}




// 
//            $(function () {
//                $('#datetimepicker1').datetimepicker();
//            });
      
