<?php
include_once('templates/header.php');
?>

<div class="ProductBoxTp1">
	<div class="slider-area" style="display: none;">
		<div class="slider-active owl-dot-style owl-carousel">
			<div class="single-slider bg-img d-flex align-items-center justify-content-center" data-src="" style="background-image:url(assets/images/header-img01.jpg);">
				<div class="slider-content pt-100" data-aos="fade-down" data-aos-duration="500">
					<div class="slider-content-wrap slider-animated-1">
						<h2 class="animated">#minteventrental</h2>
					</div>
				</div>
			</div>
			<div class="single-slider bg-img d-flex align-items-center justify-content-center" data-src="" style="background-image:url(assets/images/header-img02.jpg);">
				<div class="slider-content pt-100">
					<div class="slider-content-wrap slider-animated-1">
						<h2 class="animated">#minteventrental</h2>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="StickyBoxContent" data-sticky_column>
		<div class="DesktopOnly" style="display: none">
			<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
				<a href="#" class="LinkTp101">New Arrivals</a>
				<a href="#" class="LinkTp101">Seatings</a>
				<a href="#" class="LinkTp101">Tables</a>
				<a href="#" class="LinkTp101">Bars & Food Stations</a>
				<a href="#" class="LinkTp101">Decor</a>
				<a href="#" class="LinkTp101">Hospitality</a>
			</div>
		</div>
		<div class="MobOnly">
			<div class="ProductNavMainBox">
				<div class="ProductNavMainBoxRow1">
					<div class="d-flex d-flex align-items-center justify-content-between flex-wrap fullWidthHeight">
						<div class="LinkTp102 productNavTp1Handler">
							Catalogue
						</div>
						<div class="ProductSubNavBox">
							<div>
								<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
									<div class=" SubNavActivated d-inline-block LinkTp101">New Arrivals</div>
									<div class="ProductSubNavBoxTp1">
										<div>
											<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
												<a href="#" class="LinkTp101 SubNavActivated">Dreamscape</a>
												<a href="#" class="LinkTp101 SubNavActivated">Exotic Nomad</a>
												<a href="#" class="LinkTp101 SubNavActivated">Imperial</a>
												<a href="#" class="LinkTp101 SubNavActivated">Mexicola</a>
												<a href="#" class="LinkTp101 SubNavActivated">Dark & Moody</a>
												<a href="#" class="LinkTp101 SubNavActivated">Retro Remix</a>
												<a href="#" class="LinkTp101 SubNavActivated">French Cost</a>
												<a href="#" class="LinkTp101 SubNavActivated">White Bliss</a>
												<a href="#" class="LinkTp101 SubNavActivated">Luxe Style</a>
											</div>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Seating</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Tables</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Bars & Food Stations</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Decor</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="LinkTp102 productNavTp1Handler">
							Themes
						</div>
						<div class="ProductSubNavBox">
							<div>
								<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
									<div class=" SubNavActivated d-inline-block LinkTp101">New Arrivals</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Dreamscape</a>
											<a href="#" class="LinkTp101 SubNavActivated">Exotic Nomad</a>
											<a href="#" class="LinkTp101 SubNavActivated">Imperial</a>
											<a href="#" class="LinkTp101 SubNavActivated">Mexicola</a>
											<a href="#" class="LinkTp101 SubNavActivated">Dark & Moody</a>
											<a href="#" class="LinkTp101 SubNavActivated">Retro Remix</a>
											<a href="#" class="LinkTp101 SubNavActivated">French Cost</a>
											<a href="#" class="LinkTp101 SubNavActivated">White Bliss</a>
											<a href="#" class="LinkTp101 SubNavActivated">Luxe Style</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Seating</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Tables</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Bars & Food Stations</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Decor</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="LinkTp102 productNavTp1Handler">
							Collections
						</div>
						<div class="ProductSubNavBox">
							<div>
								<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
									<div class=" SubNavActivated d-inline-block LinkTp101">New Arrivals</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Dreamscape</a>
											<a href="#" class="LinkTp101 SubNavActivated">Exotic Nomad</a>
											<a href="#" class="LinkTp101 SubNavActivated">Imperial</a>
											<a href="#" class="LinkTp101 SubNavActivated">Mexicola</a>
											<a href="#" class="LinkTp101 SubNavActivated">Dark & Moody</a>
											<a href="#" class="LinkTp101 SubNavActivated">Retro Remix</a>
											<a href="#" class="LinkTp101 SubNavActivated">French Cost</a>
											<a href="#" class="LinkTp101 SubNavActivated">White Bliss</a>
											<a href="#" class="LinkTp101 SubNavActivated">Luxe Style</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Seating</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Tables</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Bars & Food Stations</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
									<div class=" SubNavActivated d-inline-block LinkTp101">Decor</div>
									<div class="ProductSubNavBoxTp1">
										<div class="d-flex align-items-center justify-content-center flex-wrap StickyNavBox ">
											<a href="#" class="LinkTp101 SubNavActivated">Bars & Food Stations</a>
											<a href="#" class="LinkTp101 SubNavActivated">New Arrivals</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Seatings</a>
											<a href="#" class="LinkTp101 SubNavActivated">Tables</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="gallery-area ContentBlockTp1">
		<div class="container-fluid">
			<section class="row" data-aos="fade-up" data-aos-duration="600">
				<div class="arrivaldate">
					NOVEMBER <span>2019</span>
				</div>
			</section>
			<section class="row bborder HorizontalSliderActive">
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2 ModalClickHandler" data-product-id="1" data-aos="fade-up" data-aos-duration="600">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li>
										<a href="#" class="ColorCircle Color1">&nbsp;</a>
									</li>
									<li>
										<a href="#" class="ColorCircle Color2">&nbsp;</a>
									</li>
									<li>
										<a href="#" class="ColorCircle Color3">&nbsp;</a>
									</li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2 ModalClickHandler" data-product-id="2" data-aos="fade-up" data-aos-duration="600">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li>
										<a href="#" class="ColorCircle">&nbsp;</a>
									</li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2 ModalClickHandler" data-product-id="3" data-aos="fade-up" data-aos-duration="600">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2 ModalClickHandler" data-product-id="4" data-aos="fade-up" data-aos-duration="600">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors

							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2 ModalClickHandler" data-product-id="5" data-aos="fade-up" data-aos-duration="600">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2 ModalClickHandler" data-product-id="6" data-aos="fade-up" data-aos-duration="600">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors

							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
			</section>
			<section class="row" data-aos="fade-up" data-aos-duration="600">
				<div class="olddate">
					June <span>2019</span>
				</div>
			</section>

			<section class="row" data-aos="fade-up" data-aos-duration="600">
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors

							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors

							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>

					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors

							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors

							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>

			</section>
			<section class="row" data-aos="fade-up" data-aos-duration="600">
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>

								</ul>

							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors

							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
			</section>
			<section class="row" data-aos="fade-up" data-aos-duration="600">
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
				<aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
					<div class="featureebox">
						<div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
							<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
							<h2>RENEE DINING CHAIR</h2>
							<div class="colorbox">
								<div class="newtag"><img src="assets/images/newtag.png" alt="" /></div>
								Various Colors
							</div>
							<div class="colordots">
								<ul>
									<li><img src="assets/images/dot01.jpg" alt="" /></li>
									<li><img src="assets/images/dot02.jpg" alt="" /></li>
									<li><img src="assets/images/dot03.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="selectcolor">(Select A Color)</div>
						</div>
					</div>
				</aside>
			</section>
		</div>
	</div>
</div>
<div class="NoProductBox" data-aos="fade-up" data-aos-duration="600">
	<div class="text-center">
		<div class="NoProduCtImage">
			<img src="assets/images/noproductfound.png" alt="">
		</div>
		<div class="NoProductBoxTxt"><span>Sorry !</span>We Couldnt find anything matching your search</div>
	</div>
</div>
<div class="modal fade" id="ProductDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered  modal-lg ProductDetailModalBox" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="ProductDetailPage position-relative box-placeholder" style="display: none;">
					<div class="container">
						<div class="card">
							<div class="container-fliud">
								<div class="wrapper row">
									<div class="preview col-md-6">
										<div class="imgPlaceHolder">&nbsp;</div>
										<ul class="preview-thumbnail nav nav-tabs">
											<li class="active">
												<div class="imgGrid">&nbsp;</div>
											</li>
											<li><div class="imgGrid">&nbsp;</div></li>
											<li><div class="imgGrid">&nbsp;</div></li>
											<li><div class="imgGrid">&nbsp;</div></li>
										</ul>
									</div>
									<div class="details col-md-6">
										<div class="textPlceholder1">&nbsp;</div>
										<div class="textPlceholder2">&nbsp;</div>
										<div class="textPlceholder1" style="margin-bottom:30px;">&nbsp;</div>
										
										<section class="row" style="margin-bottom:150px;">
											<aside class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
												<div class="textPlceholder1" style="width: 50%">&nbsp;</div>
												<div class="textPlceholder1" style="width: 70%">&nbsp;</div>
											</aside>
											<aside class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
												<div class="textPlceholder1" style="width: 40%">&nbsp;</div>
												<div class="textPlceholder1" style="width: 80%">&nbsp;</div>
											</aside>
										</section>
										<div class="textPlceholder2" style="border-radius:25px; line-height:2em; height: 5	0px;">&nbsp;</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="ProductDetailPage position-relative">
					<div class="ModalCloseBtn" class="close" data-dismiss="modal" aria-label="Close">
						<img src="assets/images/modalclose.png" alt="">
					</div>
					<div class="container">
						<div class="card">
							<div class="container-fliud">
								<div class="wrapper row">
									<div class="col-md-6">
										<div class="ProductDetailSliderBox">
											<div class="exzoom OpacityMinus" id="exzoom">
												<div class="exzoom_img_box">
													<ul class='exzoom_img_ul'>
														<li><img src="assets/images/Barrel-Buffet-Table.jpg" /></li>
														<li><img src="assets/images/Urban-single-seater-sofa.jpg" /></li>
														<li><img src="assets/images/Urban-Three-Seater-Sofa.gif" /></li>
														<li><img src="assets/images/titan-chair-213-Copy.jpg" /></li>
														<li><img src="assets/images/titan-chair-213-Copy.jpg" /></li>
													</ul>
												</div>
												<div class="exzoom_nav"></div>
												<div class="exzoom_btn">
													<a href="javascript:void(0);" class="exzoom_prev_btn">
														<i class="fa fa-angle-left" aria-hidden="true"></i>
													</a> 
													<a href="javascript:void(0);" class="exzoom_next_btn">
														<i class="fa fa-angle-right" aria-hidden="true"></i>
													</a>
												</div>
											</div>
										</div>
									</div>
									<div class="details col-md-6">
										<div class="breadcrumb">
											<ul>
												<li><a href="#">Catalog</a></li>
												<li><i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
												<li><a href="#">Seating</a></li>
												<li><i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
												<li><a href="#">Dinning Chairs</a></li>
											</ul>
										</div>
										<h3 class="product-title">RENEE DINING CHAIR</h3>
										<h4>Description</h4>
										<p>Leather curve sofa ideal for an unconventional style of seating Custom colors and fabrics available upon request</p>
										<section class="row">
											<aside class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
												<h4>Materials</h4>
												<p>Velvet Fabric / Stainless Steel</p>
											</aside>
											<aside class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
												<h4>Dimensions</h4>
												<p>60cm x 180cm x 90cm</p>
											</aside>
										</section>
										<div class="row">
											<div class="col-md-12">
												<h4>Color</h4>
												<div class="CstSelectBox">
													<div class="ColorSelectedBox">
														<div class="FlexBoxTp1 SelectClickHandler">
															<div class="PaletteColorBox" style="background:red !important;">&nbsp;</div>
															<div class="PaletTextBox">Selected Color</div>
															<div class="PaletteImgBox"><img src="assets/images/dropdown-arrow.jpg" alt="assets/images/dropdown-arrow.jpg"></div>
														</div>
													</div>
													<div class="OptionBoxWrp">
														<div class="OptionBoxTp1">
															<div class="FlexBoxTp1">
																<div class="PaletteColorBox ColorClickHandler" data-color="#000" style="background:black !important;">&nbsp;</div>
																<div class="PaletteColorBox ColorClickHandler" data-color="red" style="background:red !important;">&nbsp;</div>
																<div class="PaletteColorBox ColorClickHandler" data-color="green" style="background:green!important;">&nbsp;</div>
																<div class="PaletteColorBox ColorClickHandler" data-color="purple" style="background:purple !important;">&nbsp;</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<h4>Quantity</h4>
										<div class="qtyindes">
											<div class="btn-minuss"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
											<input type="text" value="1" />
											<div class="btn-pluss"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
										</div>
										<div>
											<a href="#" class="AddToQuoteBtn">Add to quote</a>
										</div>
										<div class="action">
											<a hidden="#" class="addtoquote-btn">
												<div class="inner"></div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<div class="ProductDetailPage position-relative">
					<div class="ModalCloseBtn" class="close" data-dismiss="modal" aria-label="Close">
						<img src="assets/images/modalclose.png" alt="">
					</div>
					<div class="container">
						<div class="card">
							<div class="container-fliud">
								<div class="wrapper row">
									<div class="preview col-md-6">

										<div class="preview-pic tab-content">
											<div class="tab-pane active" id="pic-1"><img class="DetailImageTp1" src="assets/images/prodimgb01.jpg" alt="Chair 01" /></div>
											<div class="tab-pane" id="pic-2"><img class="DetailImageTp1" src="assets/images/prodimgb02.jpg" alt="Chair 02" /></div>
											<div class="tab-pane" id="pic-3"><img class="DetailImageTp1" src="assets/images/prodimgb01.jpg" alt="Chair 01" /></div>
											<div class="tab-pane" id="pic-4"><img class="DetailImageTp1" src="assets/images/prodimgb02.jpg" alt="Chair 02" /></div>
										</div>
										<ul class="preview-thumbnail nav nav-tabs">
											<li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="assets/images/prodimgs01.jpg" alt="Chair 01" /></a></li>
											<li><a data-target="#pic-2" data-toggle="tab"><img src="assets/images/prodimgs02.jpg" alt="Chair 02" /></a></li>
											<li><a data-target="#pic-3" data-toggle="tab"><img src="assets/images/prodimgs01.jpg" alt="Chair 01" /></a></li>
											<li><a data-target="#pic-4" data-toggle="tab"><img src="assets/images/prodimgs02.jpg" alt="Chair 02" /></a></li>
											<!--						  <li><a data-target="#pic-5" data-toggle="tab"><img src="assets/images/prodimgs01.jpg" /></a></li>-->
										</ul>
									</div>
									<div class="details col-md-6">

										<div class="breadcrumb">
											<ul>
												<li><a href="#">Catalog</a></li>
												<li><i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
												<li><a href="#">Seating</a></li>
												<li><i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
												<li><a href="#">Dinning Chairs</a></li>


											</ul>
										</div>
										<h3 class="product-title">RENEE DINING CHAIR</h3>

										<h4>Description</h4>

										<p>Leather curve sofa ideal for an unconventional style of seating Custom colors and fabrics available upon request</p>


										<section class="row">
											<aside class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
												<h4>Materials</h4>
												<p>Velvet Fabric / Stainless Steel</p>
											</aside>

											<aside class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
												<h4>Dimensions</h4>
												<p>60cm x 180cm x 90cm</p>
											</aside>


										</section>

										<h4>Color</h4>

										<div class="colorpricebox">
											<section class="row">
												<aside class="col-6 col-xs-12 col-sm-6 col-md-6 col-lg-6">
													<div class="colorbox">
														<div class="colorspot">
															<img src="assets/images/color-selected.png" alt="ColorSpot" />
															Salmon Pink

														</div>

													</div>
												</aside>
												<aside class="col-6 col-xs-12 col-sm-6 col-md-6 col-lg-6">
													<select class="selectionbox">
														<option selected value="295">AED 295</option>
														<option value="395">AED 395</option>
														<option value="495">AED 495</option>

													</select>
												</aside>

											</section>





										</div>

										<h4>Quantity</h4>

										<div class="qtyindes">
											<div class="btn-minuss"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
											<input type="text" value="1" />
											<div class="btn-pluss"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
										</div>
										<div>
											<a href="#" class="AddToQuoteBtn">Add to quote</a>
										</div>




										<div class="action">
											<a hidden="#" class="addtoquote-btn">
												<div class="inner"></div>
											</a>
											<!--
							<button class="add-to-cart btn btn-default" type="button">add to cart</button>
							<button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button>
						</div>
-->
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

					<div class="moreproduct">
						<div class="container-fluid">
							<h2>Looks Great with...</h2>

							<div class="ProductBoxTp1 m-0">
								<div class="ArrowMiddle">
									<div class="owl-carousel owl-theme ProductBoxTp1Carousel">

										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="list_carousel">
								<div id="control-box2">
									<a class="prev3 prevv" href="#"></a>
									<a class="next3 nextt" href="#"></a>
								</div>
								<ul id="foo33">
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair02.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair02.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair02.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
								</ul>
								<div class="clear"></div>
								<div id="control-box1">
									<a class="prev3 prev" href="#"></a>
									<a class="next3 next" href="#"></a>
								</div>
							</div> -->

							<h2>Looks Great with...</h2>
							<div class="ProductBoxTp1 m-0">
								<div class="ArrowMiddle">
									<div class="owl-carousel owl-theme ProductBoxTp1Carousel">

										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="item">
											<div class="mproductbox">
												<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
												<div class="ProductTextTp1">RENEE DINING CHAIR</div>
												<div class="clear"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- <div class="list_carousel clearfix">
								<div id="control-box02">
									<a class="prev3 prevv" href="#"></a>
									<a class="next3 nextt" href="#"></a>
								</div>
								<ul id="foo34">
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair02.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair02.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
									</li>
									<li>
										<div class="mproductbox">
											<figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair02.jpg" alt="Dinning Chair"></figure>
											RENEE DINING CHAIR
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
									</li>
								</ul>
								<div class="clearfix"></div>
								<div id="control-box002">
									<a class="prev03 prev" href="#"></a>
									<a class="next03 next" href="#"></a>
								</div>
							</div> -->
						</div>


					</div>
				</div>
			</div>
				
			</div>
		</div>
	</div>
</div>
<?php
include_once('templates/footer.php');
?>