<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Mint Event Rental</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSS here -->
	<!-- all css here -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">

	<link rel="stylesheet" href="assets/css/icons.css">
	<link rel="stylesheet" href="assets/css/plugins.css">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
	<script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
	<link href="assets/images/mlogo.png" rel="apple-touch-icon-precomposed" sizes="48x48">
	<link href="assets/images/mlogo.png" rel="apple-touch-icon-precomposed">
	<link href="assets/images/mlogo.png" rel="shortcut icon">
</head>

<body>
	<div class="overlay"></div>
	<div class="formarea">
		<div class="chatbot-area">
			<div class="closeBtn1">
				<div class="mdiv">
					<div class="md"></div>
				</div>
			</div>
			<div class="imgboxx"><img src="assets/images/chat-img01.png" alt="User Image" /></div>
			<div class="chat-inner">

				<h2><span>Hey</span> Beverly</h2>
				<h3>No stress, this will only take 30 seconds</h3>

				<div class="chatbox-inner">
					<div class="chatleft clearfix">




						<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>



						<div class="ctextbox">
							What is your name ?
						</div>







					</div>


					<div class="chatright clearfix">





						<div class="ctextbox">
							Beverly
						</div>







					</div>


					<div class="chatleft clearfix">




						<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>



						<div class="ctextbox">
							What is your email address ?
						</div>







					</div>

					<div class="chatright clearfix">





						<div class="ctextbox">
							beverly@gameofthrones.com
						</div>







					</div>


					<div class="chatleft clearfix">



						<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>



						<div class="ctextbox">
							What date would you like to have the furniture delivered on?
						</div>








					</div>




					<hr />


					<div class="bchatboxx">

						<form>



							<input type="text" name="chatbox" placeholder="Type your answer here..." />


							<input type="image" name="submit" src="assets/images/chat-icon-send.png" onmouseover="javascript:this.src='assets/images/chat-icon-send.png';" onmouseout="javascript:this.src='assets/images/chat-icon-send.png';" alt="Submit">


						</form>

					</div>

					<div class="clear"></div>
				</div>
			</div>





		</div>
	</div>
	<div>

	</div>
</body>

</html>