<?php
include_once('templates/header.php');
?>
<div class="CartBoxTp1 FixedToNavBottom placeOrderBox">
	<div class="content-area-cart">
		<div class="NoProductBox" data-aos="fade-up" data-aos-duration="600" style="display: none;">
			<div class="text-center">
				<div class="NoProduCtImage">
					<img src="assets/images/noproductfound.png" alt="">
				</div>
				<div class="NoProductBoxTxt1"><span>Your</span>Cart Is Empty</div>
			</div>
		</div>
		<div class="container fixedWidth">
			<section class="row">
				<aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8">
					<h1 data-aos="fade-up">Place&nbsp;<span>order,</span></h1>
					<h3 data-aos="fade-up" data-aos-duration="600">Here are the items you added to your cart</h3>
					<div class="ProDucItemBoxWrp">
						<div class="productbox clearfix" data-aos="fade-up" data-aos-duration="600">
                            <div class="PlaceOrderItem">
                                <div class="OrderBoxTp1">
                                    <span>1</span>
                                </div>
                                <div class="OrderBoxTp2">
                                    <img src="assets/images/product01.png" alt="Product 01">
                                </div>
                                <div class="OrderBoxTp3">
                                   <div>
                                       <div class="OrderBoxTp3Txt1"> RENEE DINING CHAIR - SALMON PINK + PINK SEATPAD</div>
                                       <div class="OrderBoxTp3Txt2">Qty-12</div>
                                   </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="productbox clearfix" data-aos="fade-up" data-aos-duration="600">
                            <div class="PlaceOrderItem">
                                <div class="OrderBoxTp1">
                                    <span>2</span>
                                </div>
                                <div class="OrderBoxTp2">
                                    <img src="assets/images/product01.png" alt="Product 01">
                                </div>
                                <div class="OrderBoxTp3">
                                   <div>
                                       <div class="OrderBoxTp3Txt1"> RENEE DINING CHAIR - SALMON PINK + PINK SEATPAD</div>
                                       <div class="OrderBoxTp3Txt2">Qty-12</div>
                                   </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="productbox clearfix" data-aos="fade-up" data-aos-duration="600">
                            <div class="PlaceOrderItem">
                                <div class="OrderBoxTp1">
                                    <span>3</span>
                                </div>
                                <div class="OrderBoxTp2">
                                    <img src="assets/images/product02.png" alt="Product 01">
                                </div>
                                <div class="OrderBoxTp3">
                                   <div>
                                       <div class="OrderBoxTp3Txt1"> RENEE DINING CHAIR - SALMON PINK + PINK SEATPAD</div>
                                       <div class="OrderBoxTp3Txt2">Qty-12</div>
                                   </div>
                                </div>
                                
                            </div>
                        </div>
                        
					</div>
				</aside>
				<aside class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4" data-aos="fade-up" data-aos-duration="600">
					<div class="cart-right-detailbox clearfix">
						<div class="toptag">
							<div class="inner"></div>
						</div>
						<div class="detailinner clearfix">
							<h3>Hire Details</h3>

							<div class="txtbox">
								<div class="iconbox"><img src="assets/images/calender-icon.png" alt="" /></div>
								<!-- <input id="dom-id"  size="60" value="2015/08/09"> -->
								<input type="text" name="reservation" id="reservation" value="03/18/2013 - 03/23/2013" />
							</div>
							<div class="radioarea clearfix">
								<div>
									<label class="radio">
										<input type="radio" name="r" value="1" checked>
										<span>Delivery</span>
									</label>
									<label class="radio">
										<input type="radio" name="r" value="2">
										<span>Pick Up</span>
									</label>
								</div>
							</div>
							<div class="txtbox">
								<div class="iconbox"><img src="assets/images/location-icon.png" alt="" /></div>
								<input type="text" name="location" id="location" onfocus="this.value=''" onblur="this.value='Delivery Location'" value="Delivery Location" />
							</div>
						</div>
						<div class="linebox"></div>
						<div class="bottomdetail">
							<section class="row">
								<aside class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
									 <span style="font-family: 'airbnb_cereal_appmedium';">Hire Dates</span>
									
								</aside>
								<aside class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-right">
									1 Day
								</aside>
							</section>
							<section class="row">
								<aside class="col-12 col-sm-8 col-md-8 col-lg-8 col-xl-8">
									<span style="font-family: 'airbnb_cereal_appmedium';">Hire Items</span>
									
								</aside>
								<aside class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-right">
									42 Items
								</aside>
							</section>
							<a href="#" data-toggle="modal" data-target="#CartModal" class="continue-btn text-uppercase text-center transitionBox">Continue</a>
						</div>
					</div>
				</aside>
			</section>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="CartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered CartModalBox" role="document">
		<div class="modal-content">
			<div class="modal-body p-0">
				<div class="formarea">
					<div class="chatbot-area">
						<div class="closeBtn1" data-dismiss="modal" aria-label="Close">
							<div class="mdiv">
								<div class="md"></div>
							</div>
						</div>
						<div class="chat-inner">
							<div class="FigureBoxTp1"><img src="assets/images/chat-img01.png" alt="User Image" /></div>
							<h2 data-aos="fade-up" data-aos-duration="600"><span>Hey</span> Beverly</h2>
							<h3 data-aos="fade-up" data-aos-duration="600">No stress, this will only take 30 seconds</h3>
							<div class="chatbox-inner">
								<div class="p-4">
									<div class="BXChatTp1">
										<div class="chatleft clearfix">
											<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>
											<div class="ctextbox">
												What is your name ?
											</div>
										</div>
										
										<div class="chatright clearfix TypingBox" style="display: none">
											<div class="ctextbox">
												Typing..
											</div>
										</div>
									</div>
									<div class="BXChatTp1" style="display: none">
										<div class="chatleft clearfix">
											<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>
											<div class="ctextbox">
												What is your email address ?
											</div>
										</div>
										<div class="chatright clearfix">
											<div class="ctextbox">
												beverly@gameofthrones.com
											</div>
										</div>
									</div>
									<div class="BXChatTp1" style="display: none">
										<div class="chatleft clearfix">
											<figure class="imgleft"><img src="assets/images/chats-img01.png" alt="ddd" /></figure>
											<div class="ctextbox">
												What date would you like to have the furniture delivered on?
												<input style="display: none;" type="text" placeholder="Pick a date from here.." name="reservation1" id="reservation1" value="PICK A DATE">
												<span id="PickDateBox" class="PickDateBox  DesktopOnly">
													<img src="assets/images/pickadate.png" alt="">
												</span>
											</div>
										</div>
										<div class="chatright clearfix">
											<div class="ctextbox">
												03/18/2013 - 03/23/2013
											</div>
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="bchatboxx">
									<div class="InputBoxTp1">
										<input class="ChatInputBox" type="text" name="chatbox" placeholder="Type your answer here..." />
										<button class="SubmitBtn1" type="submit"><img src="assets/images/chat-icon-send.png" alt=""></button>
										<script>
											
										</script>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once('templates/footer.php');
?>