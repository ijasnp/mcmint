<?php
include_once('templates/header.php');
?>
<div class="slider-area">
   <div class="slider-active owl-dot-style owl-carousel">
      <div class="single-slider bg-img d-flex align-items-center justify-content-center" data-src="" style="background-image:url(assets/images/header-img01.jpg);">
         <div class="slider-content pt-100" data-aos="fade-down" data-aos-duration="500">
            <div class="slider-content-wrap slider-animated-1">
               <h2 class="animated">#minteventrental</h2>
            </div>
         </div>
      </div>
      <div class="single-slider bg-img d-flex align-items-center justify-content-center" data-src="" style="background-image:url(assets/images/header-img02.jpg);">
         <div class="slider-content pt-100">
            <div class="slider-content-wrap slider-animated-1">
               <h2 class="animated">#minteventrental</h2>
            </div>
         </div>
      </div>

   </div>
</div>
<div class="SiteContentBox">

   <div class="BoxTpArrival position-relative new-arrival-area" data-aos="fade-up" data-aos-duration="600">
      <div class="container">
         <div class="MobOnly SwipingBoxTp1" data-sticky_column>
            <div class="MobOnly">
               <div>
                  <div>
                     <div class="HorizontalSwiperBox">
                        <div class="HorizontalSwiperBoxContainer">
                           <div class="BoxContainer">
                              <a href="products.php" class="MobLinkTp1">
                                 <div class="grid-icon grid-icon--line">
                                    <span class="layer layer--primary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--secondary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--tertiary">
                                       <span></span><span></span><span></span>
                                    </span>
                                 </div>
                                 <div class="text-center TxtTpMob1">
                                    Catalogue
                                 </div>
                              </a>
                           </div>
                           <div class="BoxContainer">
                              <a href="products.php" class="MobLinkTp1">
                                 <div class="grid-icon grid-icon--line">
                                    <span class="layer layer--primary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--secondary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--tertiary">
                                       <span></span><span></span><span></span>
                                    </span>
                                 </div>
                                 <div class="text-center TxtTpMob1">
                                    Themes
                                 </div>
                              </a>
                           </div>
                           <div class="BoxContainer">
                              <a href="products.php" class="MobLinkTp1">
                                 <div class="grid-icon grid-icon--line">
                                    <span class="layer layer--primary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--secondary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--tertiary">
                                       <span></span><span></span><span></span>
                                    </span>
                                 </div>
                                 <div class="text-center TxtTpMob1">
                                    Collections
                                 </div>
                              </a>
                           </div>
                           <div class="BoxContainer">
                              <a href="products.php" class="MobLinkTp1">
                                 <div class="grid-icon grid-icon--line">
                                    <span class="layer layer--primary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--secondary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--tertiary">
                                       <span></span><span></span><span></span>
                                    </span>
                                 </div>
                                 <div class="text-center TxtTpMob1">
                                    Gallery
                                 </div>
                              </a>
                           </div>
                           <div class="BoxContainer">
                              <a href="products.php" class="MobLinkTp1">
                                 <div class="grid-icon grid-icon--line">
                                    <span class="layer layer--primary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--secondary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--tertiary">
                                       <span></span><span></span><span></span>
                                    </span>
                                 </div>
                                 <div class="text-center TxtTpMob1">
                                    Downloads
                                 </div>
                              </a>
                           </div>
                           <div class="BoxContainer">
                              <a href="products.php" class="MobLinkTp1">
                                 <div class="grid-icon grid-icon--line">
                                    <span class="layer layer--primary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--secondary">
                                       <span></span><span></span><span></span>
                                    </span>
                                    <span class="layer layer--tertiary">
                                       <span></span><span></span><span></span>
                                    </span>
                                 </div>
                                 <div class="text-center TxtTpMob1">
                                    Cart
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="MobOnly MobileTxtTp1 clearfix">New Arrivals <a href="#">Explore</a></div>
         <div class="MobileArrangeBxTp1">
            <div class="MobOnly HomeSubSliderTp1 gallery-area ContentBlockTp1">
               <div class="container-fluid">
                  <section class="row bborder HorizontalSliderActive" data-aos="fade-up" data-aos-duration="1100">
                     <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                        <div class="featureebox">
                           <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                              <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
                              <h2>RENEE DINING CHAIR</h2>
                              <div class="colorbox">
                                 Various Colors
                              </div>
                              <div class="colordots">
                                 <ul>
                                    <li><img src="assets/images/dot01.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot02.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot03.jpg" alt="" /></li>
                                 </ul>

                              </div>
                              <div class="selectcolor">(Select A Color)</div>

                              <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                           </div>
                        </div>
                     </aside>
                     <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                        <div class="featureebox">
                           <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                              <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
                              <h2>RENEE DINING CHAIR</h2>
                              <div class="colorbox">

                                 Various Colors

                              </div>
                              <div class="colordots">
                                 <ul>
                                    <li><img src="assets/images/dot01.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot02.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot03.jpg" alt="" /></li>
                                 </ul>
                              </div>
                              <div class="selectcolor">(Select A Color)</div>

                              <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->


                           </div>
                        </div>
                     </aside>
                     <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                        <div class="featureebox">
                           <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                              <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
                              <h2>RENEE DINING CHAIR</h2>
                              <div class="colorbox">

                                 Various Colors

                              </div>
                              <div class="colordots">
                                 <ul>
                                    <li><img src="assets/images/dot01.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot02.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot03.jpg" alt="" /></li>

                                 </ul>

                              </div>
                              <div class="selectcolor">(Select A Color)</div>
                              <div></div>
                              <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                           </div>

                        </div>
                     </aside>
                     <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                        <div class="featureebox">
                           <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                              <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
                              <h2>RENEE DINING CHAIR</h2>
                              <div class="colorbox">

                                 Various Colors

                              </div>
                              <div class="colordots">
                                 <ul>
                                    <li><img src="assets/images/dot01.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot02.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot03.jpg" alt="" /></li>

                                 </ul>

                              </div>
                              <div class="selectcolor">(Select A Color)</div>

                              <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                           </div>
                        </div>
                     </aside>
                     <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                        <div class="featureebox">
                           <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                              <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair01.jpg" alt="Chair" /></figure>
                              <h2>RENEE DINING CHAIR</h2>
                              <div class="colorbox">

                                 Various Colors
                              </div>
                              <div class="colordots">
                                 <ul>
                                    <li><img src="assets/images/dot01.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot02.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot03.jpg" alt="" /></li>

                                 </ul>

                              </div>
                              <div class="selectcolor">(Select A Color)</div>

                              <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                           </div>
                        </div>
                     </aside>
                     <aside class="col-6 col-sm-4 col-md-2 col-lg-2 col-xl-2">
                        <div class="featureebox">
                           <div class="FeaturedHoverBox" data-toggle="modal" data-target="#ProductDetailModal">
                              <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/chair02.jpg" alt="Chair" /></figure>
                              <h2>RENEE DINING CHAIR</h2>
                              <div class="colorbox">

                                 Various Colors

                              </div>
                              <div class="colordots">
                                 <ul>
                                    <li><img src="assets/images/dot01.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot02.jpg" alt="" /></li>
                                    <li><img src="assets/images/dot03.jpg" alt="" /></li>

                                 </ul>

                              </div>
                              <div class="selectcolor">(Select A Color)</div>

                              <!--<a href="product-details.php" class="addtocart-btn">
                              <div class="inner"></div>
                           </a>-->
                           </div>
                        </div>
                     </aside>


                  </section>
               </div>
            </div>
            <section class="row align-items-center mobileBgWhite DesktopOnly">
               <aside class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
                  <div class="ArrowMiddle">
                     <div class="owl-carousel owl-theme newArrivalSlider">
                        <div class="item">
                           <div class="slidebox">
                              <figure><img src="assets/images/arrival01.jpg" alt="My Logo"></figure>
                              <h4>Scandinavian<br>
                                 Dining chair
                              </h4>
                              <p>2 Colors</p>
                              <div class="BottomLinkBox">
                                 <a href="productdetails.php" class="addtocart-btn-tp-1">
                                    <img src="assets/images/addtocartbtn02.png" alt="">
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="slidebox">
                              <figure><img src="assets/images/arrival02.jpg" alt="My Logo"></figure>
                              <h4>Scandinavian<br>
                                 Dining chair
                              </h4>
                              <p>2 Colors</p>
                              <div class="BottomLinkBox">
                                 <a href="productdetails.php" class="addtocart-btn-tp-1">
                                    <img src="assets/images/addtocartbtn02.png" alt="">
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="slidebox">
                              <figure><img src="assets/images/arrival03.jpg" alt="My Logo"></figure>
                              <h4>Scandinavian<br>
                                 Dining chair
                              </h4>
                              <p>2 Colors</p>
                              <div class="BottomLinkBox">
                                 <a href="productdetails.php" class="addtocart-btn-tp-1">
                                    <img src="assets/images/addtocartbtn02.png" alt="">
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="slidebox">
                              <figure><img src="assets/images/arrival01.jpg" alt="My Logo"></figure>
                              <h4>Scandinavian<br>
                                 Dining chair
                              </h4>
                              <p>2 Colors</p>
                              <div class="BottomLinkBox">
                                 <a href="productdetails.php" class="addtocart-btn-tp-1">
                                    <img src="assets/images/addtocartbtn02.png" alt="">
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="slidebox">
                              <figure><img src="assets/images/arrival02.jpg" alt="My Logo"></figure>
                              <h4>Scandinavian<br>
                                 Dining chair
                              </h4>
                              <p>2 Colors</p>
                              <div class="BottomLinkBox">
                                 <a href="productdetails.php" class="addtocart-btn-tp-1">
                                    <img src="assets/images/addtocartbtn02.png" alt="">
                                 </a>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="slidebox">
                              <figure><img src="assets/images/arrival03.jpg" alt="My Logo"></figure>
                              <h4>Scandinavian<br>
                                 Dining chair
                              </h4>
                              <p>2 Colors</p>
                              <div class="BottomLinkBox">
                                 <a href="productdetails.php" class="addtocart-btn-tp-1">
                                    <img src="assets/images/addtocartbtn02.png" alt="">
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>


               </aside>
               <aside class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 BxTpItm1">
                  <h3>New Arrivals</h3>
                  <h2>NOVEMBER 2019</h2>
                  <a href="products.php" class="LinkTpT01">EXPLORE</a>
               </aside>
            </section>
         </div>
      </div>
   </div>
   <div class="about-area" data-aos="fade-up" data-aos-duration="600">
      <div class="caption-area">
         <h3>About Us</h3>
         <h2>We Know Rental</h2>
         <div class="text-right">
            <a href="products.php" class="LinkTpT02">SHOW ME</a>
         </div>
      </div>
   </div>
   <div class="downloads-area HomeDownloadArea" data-aos="fade-up" data-aos-duration="600">
      <div class="catalouge">Catalogue</div>
      <div class="downloadsbg">Downloads</div>
      <div class="container">
         <section class="row">
            <aside class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
               <h3>Downloads</h3>
               <h2>GET OUR Catalogue!</h2>
               <ul>
                  <section class="row">
                     <aside class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <li>Master Catalog 2018/2019</li>
                        <li>Party & Wedding Catalog 2018/2019</li>
                        <li>Exhibition Catalog 2018/2019</li>
                     </aside>
                     <aside class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <li>Conference Catalog 2018/2019</li>
                        <li>Tablescapes Catalog 2018/2019</li>
                        <li>Prop Catalog 2018/2019</li>
                     </aside>
                  </section>
               </ul>
               <div class="LinkTpT03Wrp">
                  <a href="products.php" class="LinkTpT03">DOWNLOAD ALL</a>
               </div>
            </aside>
            <aside class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
               <section class="row">
                  <aside class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                     <figure><img src="assets/images/dl-img01.jpg" alt="" /></figure>
                  </aside>
                  <aside class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                     <figure><img src="assets/images/dl-img01.jpg" alt="" /></figure>
                  </aside>
               </section>
            </aside>
         </section>
      </div>
   </div>
   <div class="range-area" data-aos="fade-up" data-aos-duration="600">
      <div class="caption-area">
         <h3>Lorem ipsum</h3>
         <h2>Morrocan Sunset</h2>
         <div class="text-right">
            <a href="products.php" class="LinkTpT02 viewTheRangeBtn">VIEW THE RANGE</a>
         </div>
      </div>
   </div>
   <div class="deliverytracking-area HomeDeliveryTrackingArea" data-aos="fade-up" data-aos-duration="600">
      <div class="deliveryl">Delivery</div>
      <div class="tranckingc">Tracking</div>
      <div class="container">
         <section class="row">
            <aside class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7">
               <h3>ON TIME, EVERY TIME</h3>
               <h2>DELIVERY TRACKING</h2>
               <ul>
                  <li>View all your orders online</li>
                  <li>Track your deliveries in real time</li>
                  <li>Receive prompt alerts</li>
               </ul>
               <p>*for corporate customers only</p>
               <a href="<contactus.php" class="partner-btn">Partner with us</a>
            </aside>
            <aside class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5">
               <figure><img src="assets/images/mobile-phone.jpg" alt="phone" /></figure>
            </aside>
         </section>
      </div>
   </div>
   <div class="bookameeting" data-aos="fade-up" data-aos-duration="600">
      <div class="caption-area">
         <h3>SEEING IS BELIEVING</h3>
         <h2>SWING BY OUR HQ</h2>
         <a href="contactus.php" class="bookameeting-btn">
            <div class="inner"></div>
         </a>
      </div>
   </div>

</div>
<?php
include_once('templates/footer.php');
?>