<?php
include_once('templates/header.php');
?>
<!-- <div class="BlogWrpBoxTp1 fixedPaddingTop" data-aos="fade-down" data-aos-duration="800">
    <div class="UnderConstructionBox">
        <div>
            <img src="assets/images/underconstruction.png" alt="">
        </div>
    </div>
</div> -->
<div class="fixedPaddingTop">
    <div class="blogPageTp1">
        <div class="container">
            <div class="blogPageTp1Content">
                <div class="blogItemBox" data-aos="fade-up" data-aos-duration="800">
                    <div class="row">
                        <div class="col-md-5">
                            <img class="blogImage" src="assets/images/blog1.jpeg" alt="assets/images/blog1.jpeg">
                        </div>
                        <div class="col-md-7">
                            <div>
                                <div class="blogPostContainer">
                                    <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                                    <span class="watermark" aria-hidden="true">O</span>
                                    <div class="postBlogTitle">One space,so many possibilities</div>
                                    <div class="postBlogSubTitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse dolorem nemo rem voluptatem delectus at minus libero id.</div>
                                    <a href="blogdetail.php" class="BlogViewAllButton">View Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blogItemBox" data-aos="fade-up" data-aos-duration="800">
                    <div class="row">
                       
                        <div class="col-md-7">
                            <div>
                                <div class="blogPostContainer">
                                    <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                                    <span class="watermark" aria-hidden="true">C</span>
                                    <div class="postBlogTitle">Camping experience besides Kinney Lake, Canada</div>
                                    <div class="postBlogSubTitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse dolorem nemo rem voluptatem delectus at minus libero id.</div>
                                    <a href="blogdetail.php" class="BlogViewAllButton">View Details</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <img class="blogImage" src="assets/images/blog2.jpeg" alt="assets/images/blog1.jpeg">
                        </div>
                    </div>
                </div>
                <div class="blogItemBox" data-aos="fade-up" data-aos-duration="800">
                    <div class="row">
                        <div class="col-md-5">
                            <img class="blogImage" src="assets/images/blog3.jpeg" alt="assets/images/blog3.jpeg">
                        </div>
                        <div class="col-md-7">
                            <div>
                                <div class="blogPostContainer">
                                    <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                                    <span class="watermark" aria-hidden="true">O</span>
                                    <div class="postBlogTitle">One space, so many possibilities</div>
                                    <div class="postBlogSubTitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse dolorem nemo rem voluptatem delectus at minus libero id.</div>
                                    <a href="blogdetail.php" class="BlogViewAllButton">View Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blogItemBox" data-aos="fade-up" data-aos-duration="800">
                    <div class="row">
                       
                        <div class="col-md-7">
                            <div>
                                <div class="blogPostContainer">
                                    <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                                    <span class="watermark" aria-hidden="true">C</span>
                                    <div class="postBlogTitle">Camping experience besides Kinney Lake, Canada</div>
                                    <div class="postBlogSubTitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse dolorem nemo rem voluptatem delectus at minus libero id.</div>
                                    <a href="blogdetail.php" class="BlogViewAllButton">View Details</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <img class="blogImage" src="assets/images/blog4.jpeg" alt="assets/images/blog1.jpeg">
                        </div>
                    </div>
                </div>
                <div class="blogItemBox" data-aos="fade-up" data-aos-duration="800">
                    <div class="row">
                        <div class="col-md-5">
                            <img class="blogImage" src="assets/images/blog1.jpeg" alt="assets/images/blog1.jpeg">
                        </div>
                        <div class="col-md-7">
                            <div>
                                <div class="blogPostContainer">
                                    <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                                    <span class="watermark" aria-hidden="true">O</span>
                                    <div class="postBlogTitle">One space, so many possibilities</div>
                                    <div class="postBlogSubTitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse dolorem nemo rem voluptatem delectus at minus libero id.</div>
                                    <a href="blogdetail.php" class="BlogViewAllButton">View Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blogItemBox" data-aos="fade-up" data-aos-duration="800">
                    <div class="row">
                       
                        <div class="col-md-7">
                            <div>
                                <div class="blogPostContainer">
                                    <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                                    <span class="watermark" aria-hidden="true">C</span>
                                    <div class="postBlogTitle">Camping experience besides Kinney Lake, Canada</div>
                                    <div class="postBlogSubTitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse dolorem nemo rem voluptatem delectus at minus libero id.</div>
                                    <a href="blogdetail.php" class="BlogViewAllButton">View Details</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <img class="blogImage" src="assets/images/blog2.jpeg" alt="assets/images/blog1.jpeg">
                        </div>
                    </div>
                </div>
                <div class="blogItemBox" data-aos="fade-up" data-aos-duration="800">
                    <div class="row">
                        <div class="col-md-5">
                            <img class="blogImage" src="assets/images/blog3.jpeg" alt="assets/images/blog3.jpeg">
                        </div>
                        <div class="col-md-7">
                            <div>
                                <div class="blogPostContainer">
                                    <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                                    <span class="watermark" aria-hidden="true">O</span>
                                    <div class="postBlogTitle">One space, so many possibilities</div>
                                    <div class="postBlogSubTitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse dolorem nemo rem voluptatem delectus at minus libero id.</div>
                                    <a href="blogdetail.php" class="BlogViewAllButton">View Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blogItemBox" data-aos="fade-up" data-aos-duration="800">
                    <div class="row">
                       
                        <div class="col-md-7">
                            <div>
                                <div class="blogPostContainer">
                                    <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                                    <span class="watermark" aria-hidden="true">C</span>
                                    <div class="postBlogTitle">Camping experience besides Kinney Lake, Canada</div>
                                    <div class="postBlogSubTitle">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse dolorem nemo rem voluptatem delectus at minus libero id.</div>
                                    <a href="blogdetail.php" class="BlogViewAllButton">View Details</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <img class="blogImage" src="assets/images/blog4.jpeg" alt="assets/images/blog1.jpeg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once('templates/footer.php');
?>