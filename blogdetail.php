<?php
include_once('templates/header.php');
?>
<div class="fixedPaddingTop">
    <div class="blogDetailBox">
        <div class="blogDetailBoxHeader" style="background-image:url(assets/images/blog3.jpeg);" data-aos="fade-up" data-aos-duration="800">
            <div>
                <time class="datePosted" datetime="2018-09-18 06:02">18 September, 2018</time>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="blogDetailBoxTxt1 text-center">Riding bicycle is environment friendly and good for health</div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
        <div class="blogDetailBoxContent">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="blogDetailBoxContentTxt1" data-aos="fade-up" data-aos-duration="800">
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose.
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose.
                        </div>
                        <div class="blogDetailBoxContentTxt1" data-aos="fade-up" data-aos-duration="800">
                            There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                        </div>
                        <div class="blogDetailBoxContentTxt1" data-aos="fade-up" data-aos-duration="800">
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                        </div>
                        <div class="blogDetailBoxContentTxt1" data-aos="fade-up" data-aos-duration="800">
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="BlogTptxt1" data-aos="fade-up" data-aos-duration="800">
                            <span>Recent Blogs</span>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog1.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Riding bicycle is environment friendly and good for health
                                            </div>
                                            <div class="blogThumbBoxTxt2">24 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog2.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Riding bicycle is environment friendly and good for health
                                            </div>
                                            <div class="blogThumbBoxTxt2">45 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog3.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Riding bicycle is environment friendly and good for health
                                            </div>
                                            <div class="blogThumbBoxTxt2">65 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog4.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Riding bicycle is environment friendly and good for health
                                            </div>
                                            <div class="blogThumbBoxTxt2">0 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog1.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Riding bicycle is environment friendly and good for health
                                            </div>
                                            <div class="blogThumbBoxTxt2">Now</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog2.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Riding bicycle is environment friendly and good for health
                                            </div>
                                            <div class="blogThumbBoxTxt2">one hour ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog3.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Riding bicycle is environment friendly and good for health
                                            </div>
                                            <div class="blogThumbBoxTxt2">12 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog4.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Riding bicycle is environment friendly and good for health
                                            </div>
                                            <div class="blogThumbBoxTxt2">12 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog4.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo inventore dicta dignissimos aut deleniti assumenda non, fuga quae? Cum itaque nisi omnis nostrum explicabo praesentium facere modi id iusto corporis?
                                            </div>
                                            <div class="blogThumbBoxTxt2">12 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="mb-2" data-aos="fade-up" data-aos-duration="800">
                            <a href="blog.php">
                                <div class="blogThumbBox">
                                    <div class="row">
                                        <div class="col-md-5 pr-0">
                                            <img class="BlogDetailImage" src="assets/images/blog4.jpeg" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="blogThumbBoxTxt1">
                                                Lorem Lipsum Sit amet angelina row amanten quackil dockle
                                            </div>
                                            <div class="blogThumbBoxTxt2">12 minutes ago</div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once('templates/footer.php');
?>