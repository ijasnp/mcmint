<?php
include_once('templates/header.php');
?>
<div class="testimonialWrpBoxTp1 fixedPaddingTop" data-aos="fade-down" data-aos-duration="600">
    <div class="testimonialArea">
        <div class="container fixedWidth">
            <h2>Testimonials</h2>
            <h4 class="SubTitleTp1">
                See what our clients have to say...we are eternally grateful to those clients who have been good enough to reccommend us
            </h4>
        </div>
        <div class="TestimonialContent">
            <!-- Testimonial Box Will comes Here -->
            <div class="container fixedWidth">
                <div class="TestimonialCarouselWrp ArrowMiddle" data-aos="fade-up" data-aos-duration="600">
                    <div class="owl-carousel owl-theme TestimonialCarousel" data-aos="fade-up" data-aos-duration="600">
                        <div class="item" data-aos="fade-up" data-aos-duration="600">
                            <div class="CompanyTestiomonial text-center">
                                <div class="TestimonialAvatar">
                                    <img src="assets/images/logo.png" alt="">
                                </div>
                                <div class="TestimonialText">
                                    <img class="left" src="assets/images/testleft.png" alt="assets/images/testleft.png">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas consectetur ratione, nesciunt et placeat sit nobis totam eius non odit nulla ducimus facere. In veniam sunt facilis cumque aut provident.
                                    <img class="right" src="assets/images/testright.png" alt="assets/images/testright.png">
                                </div>
                                <div class="TestimonialOwner">Ambrue DeAmbrue DeAmbrue De</div>
                                <div class="TestimonialOwnerProfession">Creative Manger</div>
                                <div class="TestimonialOwnerCompany">Al Ain Travels</div>
                            </div>
                        </div>
                        <div class="item" data-aos="fade-up" data-aos-duration="600">
                            <div class="CompanyTestiomonial text-center">
                                <div class="TestimonialAvatar">
                                    <img src="assets/images/logo.png" alt="">
                                </div>
                                <div class="TestimonialText">
                                    <img class="left" src="assets/images/testleft.png" alt="assets/images/testleft.png">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas consectetur ratione, nesciunt et placeat sit nobis totam eius non odit nulla ducimus facere. In veniam sunt facilis cumque aut provident.
                                    <img class="right" src="assets/images/testright.png" alt="assets/images/testright.png">
                                </div>
                                <div class="TestimonialOwner">Ambrue De</div>
                            </div>
                        </div>
                        <div class="item" data-aos="fade-up" data-aos-duration="600">
                            <div class="CompanyTestiomonial text-center">
                                <div class="TestimonialAvatar">
                                    <img src="assets/images/logo.png" alt="">
                                </div>
                                <div class="TestimonialText">
                                    <img class="left" src="assets/images/testleft.png" alt="assets/images/testleft.png">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas consectetur ratione, nesciunt et placeat sit nobis totam eius non odit nulla ducimus facere. In veniam sunt facilis cumque aut provident.
                                    <img class="right" src="assets/images/testright.png" alt="assets/images/testright.png">
                                </div>
                                <div class="TestimonialOwner">AmbrueDe</div>
                            </div>
                        </div>
                        <div class="item" data-aos="fade-up" data-aos-duration="600">
                            <div class="CompanyTestiomonial text-center">
                                <div class="TestimonialAvatar">
                                    <img src="assets/images/logo.png" alt="">
                                </div>
                                <div class="TestimonialText">
                                    <img class="left" src="assets/images/testleft.png" alt="assets/images/testleft.png">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas consectetur ratione, nesciunt et placeat sit nobis totam eius non odit nulla ducimus facere. In veniam sunt facilis cumque aut provident.
                                    <img class="right" src="assets/images/testright.png" alt="assets/images/testright.png">
                                </div>
                                <div class="TestimonialOwner">Ambrue De</div>
                            </div>
                        </div>
                        <div class="item" data-aos="fade-up" data-aos-duration="600">
                            <div class="CompanyTestiomonial text-center">
                                <div class="TestimonialAvatar">
                                    <img src="assets/images/logo.png" alt="">
                                </div>
                                <div class="TestimonialText">
                                    <img class="left" src="assets/images/testleft.png" alt="assets/images/testleft.png">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas consectetur ratione, nesciunt et placeat sit nobis totam eius non odit nulla ducimus facere. In veniam sunt facilis cumque aut provident.
                                    <img class="right" src="assets/images/testright.png" alt="assets/images/testright.png">
                                </div>
                                <div class="TestimonialOwner">Ambrue De</div>
                            </div>
                        </div>
                        <div class="item" data-aos="fade-up" data-aos-duration="600">
                            <div class="CompanyTestiomonial text-center">
                                <div class="TestimonialAvatar">
                                    <img src="assets/images/logo.png" alt="">
                                </div>
                                <div class="TestimonialText">
                                    <img class="left" src="assets/images/testleft.png" alt="assets/images/testleft.png">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas consectetur ratione, nesciunt et placeat sit nobis totam eius non odit nulla ducimus facere. In veniam sunt facilis cumque aut provident.
                                    <img class="right" src="assets/images/testright.png" alt="assets/images/testright.png">
                                </div>
                                <div class="TestimonialOwner">Ambrue De</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
include_once('templates/footer.php');
?>