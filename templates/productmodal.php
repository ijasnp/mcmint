<div class="ProductDetailPage position-relative">
    <div class="ModalCloseBtn" class="close" data-dismiss="modal" aria-label="Close">
        <img src="assets/images/modalclose.png" alt="">
    </div>
    <div class="container">
        <div class="card">
            <div class="container-fliud">
                <div class="wrapper row">
                    <div class="preview col-md-6">
                        <div class="preview-pic tab-content">
                            <div class="tab-pane active" id="pic-1"><img class="DetailImageTp1" src="assets/images/prodimgb01.jpg" alt="Chair 01" /></div>
                            <div class="tab-pane" id="pic-2"><img class="DetailImageTp1" src="assets/images/prodimgb02.jpg" alt="Chair 02" /></div>
                            <div class="tab-pane" id="pic-3"><img class="DetailImageTp1" src="assets/images/prodimgb01.jpg" alt="Chair 01" /></div>
                            <div class="tab-pane" id="pic-4"><img class="DetailImageTp1" src="assets/images/prodimgb02.jpg" alt="Chair 02" /></div>
                        </div>
                        <ul class="preview-thumbnail nav nav-tabs">
                            <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="assets/images/prodimgs01.jpg" alt="Chair 01" /></a></li>
                            <li><a data-target="#pic-2" data-toggle="tab"><img src="assets/images/prodimgs02.jpg" alt="Chair 02" /></a></li>
                            <li><a data-target="#pic-3" data-toggle="tab"><img src="assets/images/prodimgs01.jpg" alt="Chair 01" /></a></li>
                            <li><a data-target="#pic-4" data-toggle="tab"><img src="assets/images/prodimgs02.jpg" alt="Chair 02" /></a></li>
                        </ul>
                    </div>
                    <div class="details col-md-6">
                        <div class="breadcrumb">
                            <ul>
                                <li><a href="#">Catalog</a></li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                                <li><a href="#">Seating</a></li>
                                <li><i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                                <li><a href="#">Dinning Chairs</a></li>
                            </ul>
                        </div>
                        <h3 class="product-title">RENEE DINING CHAIR</h3>
                        <h4>Description</h4>
                        <p>Leather curve sofa ideal for an unconventional style of seating Custom colors and fabrics available upon request</p>
                        <section class="row">
                            <aside class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
                                <h4>Materials</h4>
                                <p>Velvet Fabric / Stainless Steel</p>
                            </aside>
                            <aside class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                                <h4>Dimensions</h4>
                                <p>60cm x 180cm x 90cm</p>
                            </aside>
                        </section>
                        <h4>Color</h4>
                        <div class="colorpricebox">
                            <section class="row">
                                <aside class="col-6 col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="colorbox">
                                        <div class="colorspot">
                                            <img src="assets/images/color-selected.png" alt="ColorSpot" />
                                            Salmon Pink
                                        </div>
                                    </div>
                                </aside>
                                <aside class="col-6 col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <select class="selectionbox">
                                        <option selected value="295">AED 295</option>
                                        <option value="395">AED 395</option>
                                        <option value="495">AED 495</option>
                                    </select>
                                </aside>
                            </section>
                        </div>
                        <h4>Quantity</h4>
                        <div class="qtyindes">
                            <div class="btn-minuss"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                            <input type="text" value="1" />
                            <div class="btn-pluss"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                        </div>
                        <div>
                            <a href="#" class="AddToQuoteBtn">Add to quote</a>
                        </div>
                        <div class="action">
                            <a hidden="#" class="addtoquote-btn">
                                <div class="inner"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="moreproduct">
        <div class="container-fluid">
            <h2>Looks Great with...</h2>
            <div class="ProductBoxTp1">
                <div class="ArrowMiddle">
                    <div class="owl-carousel owl-theme ProductBoxTp1Carousel">
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h2>Looks Great with...</h2>
            <div class="ProductBoxTp1">
                <div class="ArrowMiddle">
                    <div class="owl-carousel owl-theme ProductBoxTp1Carousel">
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="mproductbox">
                                <figure><img class="lazy" src="assets/images/ajaxloader.png" data-src="assets/images/dchair01.jpg" alt="Dinning Chair"></figure>
                                <div class="ProductTextTp1">RENEE DINING CHAIR</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>