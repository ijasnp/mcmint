<footer class="footer-area">
   <div class="container fixedWidth" data-aos="fade-up" data-aos-duration="600">
      <section class="row">
         <aside class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
            <h2 class="MobileLinkTp101">
               RENTAL <br> FURNITURE
               <i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
               <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i>
            </h2>
            <ul class="MobileBoxTp101">
               <li><a href="products.php">Chairs</a></li>
               <li><a href="products.php">Tables</a></li>
               <li><a href="products.php">Bars</a></li>
               <li><a href="products.php">Food Stations</a></li>
               <li><a href="products.php">Decor</a></li>
               <li><a href="products.php">Hospitality</a></li>
               <li><a href="products.php">Collections</a></li>
               <li><a href="products.php">Dancefloors</a></li>
               <li><a href="products.php">Outdoor</a></li>
               <li><a href="products.php">Themes</a></li>
            </ul>
         </aside>
         <aside class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
            <h2 class="MobileLinkTp101">TABLEWARE<br> RENTAL<i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
               <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i></h2>
            <ul class="MobileBoxTp101">
               <li><a href="products.php">Charger Plates</a></li>
               <li><a href="products.php">Crockery</a></li>
               <li><a href="products.php">Cutlery</a></li>
               <li><a href="products.php">Glasses</a></li>
               <li><a href="products.php">Linen</a></li>
               <li><a href="products.php">Equipment</a></li>
               <li><a href="products.php">Bar Service</a></li>
            </ul>
         </aside>
         <aside class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
            <h2 class="MobileLinkTp101">GALLERIES<i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
               <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i></h2>
            <ul class="MobileBoxTp101">
               <li><a href="products.php">Party</a></li>
               <li><a href="products.php">Wedding</a></li>
               <li><a href="products.php">Conference</a></li>
               <li><a href="products.php">Coporate Events</a></li>
               <li><a href="products.php">Major Events</a></li>
               <li><a href="products.php">Exhibitions</a></li>
            </ul>
         </aside>
         <aside class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
            <h2 class="MobileLinkTp101">GENERAL<i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
               <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i></h2>
            <ul class="MobileBoxTp101">
               <li><a href="faq.php">FAQ’s</a></li>
               <li><a href="termsandconditions.php">Rental Terms & Contitions</a></li>
               <li><a href="products.php">Conference</a></li>
               <li><a href="products.php">Coporate Events</a></li>
               <li><a href="products.php">Major Events</a></li>
               <li><a href="products.php">Exhibitions</a></li>
            </ul>
         </aside>
         <aside class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
            <h2 class="MobileLinkTp101">CONTACT US<i class="fa fa-plus PlusIcon1 IconTpMob1" aria-hidden="true"></i>
               <i class="fa fa-minus MinusIcon1 IconTpMob1" aria-hidden="true"></i></h2>
            <div class="MobileBoxTp101">
            <p> Within UAE: 800 6468 (MINT)
               <br>
               Outside UAE: +971 4 3474340
            </p>
            <p>Whatsapp: +971 55 2149122</p>
            <p>Email:<a href="mailto:sales@minteventrentals.com">sales@minteventrentals.com</a>
            </p>
            <p>MINT HQ:<a href="#">www.googlemaps.com</a>
            </p>
            <p>MINT DIC WH:<a href="#">www.googlemaps.com</a>
            </p>
            <p>MINT AUH WH:<a href="#">www.googlemaps.com</a>
            </p>
            </div>
            <div class="social-area">
               <h3>Connect With Us</h3>
               <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
               <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>

               <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
               </a>
               <div class="clear"></div>
            </div>
         </aside>
      </section>
      
      <div class="clear"></div>
   </div>
   <div class="clear"></div>
</footer>
<footer class="footer-bottom">
   <div class="container fixedWidth">
      © Copyright 2019. Mint Event Rentals. All Rights Reserved. | <a href="#">Privay Policy</a> | <a href="termsandconditions.php">Terms & Conditions</a> | <a href="#">Payment Policy</a> |<a href="faq.php">FAQ</a>
   </div>
</footer>

<!-- all js here -->
<script src="assets/js/vendor/jquery-1.12.0.min.js"></script>  
<script src="assets/js/jquery.lazy.min.js"></script>
<script src="assets/js/popper.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/ajax-mail.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/moment.js"></script>
<script src="assets/js/jquery.daterangepicker.min.js"></script>
<script src="assets/js/stickyLatest.js"></script>
<script src="assets/js/jquery.exzoom.js"></script>
<script type="text/javascript">
(function($) {
  $.fn.nodoubletapzoom = function() {
      $(this).bind('touchstart', function preventZoom(e) {
        var t2 = e.timeStamp
          , t1 = $(this).data('lastTouch') || t2
          , dt = t2 - t1
          , fingers = e.originalEvent.touches.length;
        $(this).data('lastTouch', t2);
        if (!dt || dt > 500 || fingers > 1) return; // not double-tap

        e.preventDefault(); // double tap - prevent the zoom
        // also synthesize click events we just swallowed up
        $(this).trigger('click').trigger('click');
      });
  };
})(jQuery);
$(window).scroll(function(){
  var sticky = $('.HeroLogo'),
      scroll = $(window).scrollTop();
  if (scroll >= 100) sticky.addClass('fixedHeight');
  else sticky.removeClass('fixedHeight');
});
jQuery(document).ready(function($) {


   // $('.ModalClickHandler').on('click',function(){
   //    var ProductKey = $(this).attr("data-product-id");
   //    $('.modal-body').load('templates/productmodal.php?id=2',function(){

   //       $('#ProductDetailModal').modal({show:true});
   //    });
   // });



   $(".SiteContentBox").css('display', 'none');
   $('html,body').animate({ scrollTop: 0 }, '20');
   $('.slider-active').on('changed.owl.carousel', function(event){ 
      $(".SiteContentBox").css('display', 'block');
   });


   $(".ColorSelectedBox").on("click",function(){
      $(this).parent().toggleClass("active");

   })
   $(".ColorClickHandler").on("click",function(){
      var SelectedColor= $(this).attr("data-color");
      $(".ColorSelectedBox .PaletteColorBox").css("background-color", SelectedColor);
      $(".CstSelectBox").toggleClass("active");
   })


    $('.slider-active').owlCarousel({
      callbacks: true,
        loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 5000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        //navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		navText:['<div class="smouseicon"><img src="assets/images/mouse-icon.png" alt=""/></div>'],
        item: 1,
		//navElement:'smouseicon',
        responsive: {
            0: {
                items: 1,
                autoplay: true,
                autoplayTimeout: 5000,
            },
            768: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })
});
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.BXtpHeaderTp1').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    if (st > lastScrollTop && st > navbarHeight){
        $('.BXtpHeaderTp1').removeClass('nav-down').addClass('nav-up');
        $('body').removeClass('nav-down-active').addClass('nav-up-active');
    } else {
        if(st + $(window).height() < $(document).height()) {
            $('.BXtpHeaderTp1').removeClass('nav-up').addClass('nav-down');
            $('body').removeClass('nav-up-active').addClass('nav-down-active');
        }
    }
    
    lastScrollTop = st;
}



 $(function() {

         $(".productNavTp1Handler").click(
            function(){
               $(".productNavTp1Handler").removeClass("active");
               $(".ProductSubNavBox").removeClass("active")
               $(this).toggleClass("active");
               $(this).next(".ProductSubNavBox").toggleClass("active");
               $(".overlay").addClass("active");
            }
         );
         $(".SubNavActivated").click(
            function(){
               $(".ProductSubNavBoxTp1").removeClass("active");
               $(this).next(".ProductSubNavBoxTp1").toggleClass("active")
            }
         );
         $(".overlay").click(
            function(){
               $(".overlay").removeClass("active");
               $(".productNavTp1Handler").removeClass("active");
               $(".ProductSubNavBox").removeClass("active")
               $(".ProductSubNavBoxTp1").removeClass("active");
            }
         );
         $('.add').click(function () {
               if ($(this).next().val() < 9) {
               $(this).next().val(+$(this).next().val() + 1);
            }
         });
         $('.sub').click(function () {
               if ($(this).prev().val() > 0) {
               if ($(this).prev().val() > 0) $(this).prev().val(+$(this).prev().val() - 1);
               }
         });         
        $('.lazy').Lazy({
         effect: 'fadeIn',
         effectTime: 100,
         visibleOnly: true
        });
        
    });
               $(document).ready(function() {
                  var separator = ' - ',
                  dateFormat = 'YYYY/MM/DD';
                  var options = {
                     autoUpdateInput: false,
                     autoApply: true,
                     locale: {
                           format: dateFormat,
                           separator: separator,
                           applyLabel: 'Select Date',
                           cancelLabel: 'Cancel'
                     },
                     // minDate: moment().add(1, 'days'),
                     // maxDate: moment().add(359, 'days'),
                     opens: "right"
                  };
                  $('#reservation').daterangepicker(options)
                  // $('#reservation').daterangepicker(optionSet2, function(start, end, label) {
                  //   console.log(start.toISOString(), end.toISOString(), label);
                  // });
                  
                  // var optionSet2 = {
                  //   startDate: moment().subtract('days', 7),
                  //   endDate: moment(),
                  //   opens: 'center',
                  //   ranges: {
                  //      'Today': [moment(), moment()],
                  //      'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                  //      'Last 7 Days': [moment().subtract('days', 6), moment()],
                  //      'Last 30 Days': [moment().subtract('days', 29), moment()],
                  //      'This Month': [moment().startOf('month'), moment().endOf('month')],
                  //      'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                  //   }
                  // };
                  // $('#reservation1').daterangepicker(null, function(start, end, label) {
                  //   console.log(start.toISOString(), end.toISOString(), label);
                  // });
                  // $('#PickDateBox').click(function() {
                  //    $('#reservation1').click();
                  //   $('#reservation1').data('daterangepicker').setOptions(optionSet2, cb);
                  // });
               });
               </script>
<!-- <script src="https://unpkg.com/aos@next/dist/aos.js"></script> -->

<script src="assets/js/main.js"></script>

<script>
		function google(stateWrapper, ready) {
			window.open("https://google.com");
			ready();
		}
		function bing(stateWrapper, ready) {
			window.open("https://bing.com");
			ready();
		}
		var rollbackTo = false;
		var originalState = false;
		function storeState(stateWrapper, ready) {
			rollbackTo = stateWrapper.current;
			console.log("storeState called: ",rollbackTo);
			ready();
		}
		function rollback(stateWrapper, ready) {
			console.log("rollback called: ", rollbackTo, originalState);
			console.log("answers at the time of user input: ", stateWrapper.answers);
			if(rollbackTo!=false) {
				if(originalState==false) {
					originalState = stateWrapper.current.next;
						console.log('stored original state');
				}
				stateWrapper.current.next = rollbackTo;
				console.log('changed current.next to rollbackTo');
			}
			ready();
		}
		function restore(stateWrapper, ready) {
			if(originalState != false) {
				stateWrapper.current.next = originalState;
				console.log('changed current.next to originalState');
			}
			ready();
      }
      AOS.init({
  startEvent: 'load',
  offset: 0,
  disable: 'mobile'
//   easing: 'ease-in-sine',
});
      
   </script>
   
	<!-- <script>
		jQuery(function($){
			convForm = $('#chat').convform({selectInputStyle: 'disable'});
			console.log(convForm);
		});
   </script> -->

</body>

</html>