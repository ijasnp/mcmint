<!doctype html>
<html>

<head>

   <meta charset="utf-8">
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <title>Mint Event Rental</title>
   <meta name="description" content="">
   <meta name="viewport" content="viewport-fit=cover,width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0" />
   <!-- CSS here -->
   <!-- all css here -->
   <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css">
   <link rel="stylesheet" href="assets/css/icons.css"> -->

   <!-- <link rel="stylesheet" href="assets/css/style.css">
   <link rel="stylesheet" href="assets/css/responsive.css">
   <link rel="stylesheet" href="assets/css/productpage.css">
   <link rel="stylesheet" href="assets/css/crousel.css"> -->
   <!-- <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" /> -->


   <link rel="stylesheet" href="assets/css/mintplugins.css">
   <link rel="stylesheet" href="assets/css/mastermint.css">



   <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
   <link href="assets/images/mlogo.png" rel="apple-touch-icon-precomposed" sizes="48x48">
   <link href="assets/images/mlogo.png" rel="apple-touch-icon-precomposed">
   <link href="assets/images/mlogo.png" rel="shortcut icon">
   <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png">
   <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png">
   <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
   <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
   <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">
   <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png">
   <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png">
   <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png">
   <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png">
   <link rel="icon" type="image/png" sizes="192x192" href="assets/images/android-icon-192x192.png">
   <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png">
   <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png">
   <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png">
   <link rel="manifest" href="assets/images/manifest.json">
   <meta name="msapplication-TileColor" content="#ffffff">
   <meta name="msapplication-TileImage" content="assets/images/ms-icon-144x144.png">

   <meta name="apple-mobile-web-app-capable" content="yes" />
   <meta name="theme-color" content="#33bfb4" />
   <meta name="msapplication-navbutton-color" content="#33bfb4">
   <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">


   

</head>

<body data-sticky_parent>
   <div class="overlay"></div>
   <!-- header Start Here -->
   <header class="header-area">
      <div class="BXtpHeaderTp1 DeskTopOption" id="HeaderDesktop">
         <div class="BxTp1Cnt1">
            <div class="BxTp1Left fullHeight align-items-center">
               <div class="sidemenu"><a href="#"><img class="TopBarIconTp1 mrgRight50" src="assets/images/menu-icon.png" alt="Side Menu" /></a>
                  <div id="sideNavigation" class="sidenav">
                     <div class="sidenav-inner">
                        <span class="MenuRightShape">&nbsp;</span>
                        <a href="javascript::" class="closebtn">&times;</a> <a href="about-us.php">
                           <h2>About us</h2>
                        </a>
                        <a href="download-catalog.php" class="">
                           <h2>Dowloads</h2>
                        </a>
                        <a href="cart.php">
                           <h2>Cart</h2>
                        </a>
                        <ul>
                           <li><a href="faq.php">FAQs</a></li>
                           <li><a href="pressrelease.php">press release</a></li>
                           <li><a href="blog.php">blog</a></li>
                           <li><a href="terms-and-conditions.php">terms & Conditions</a></li>
                           <li><a href="contact-us.php">Contact Us</a></li>
                           <li><a href="testimonial.php">testimonials</a></li>
                        </ul> 
                        <div class="clear"></div>
                        <div class="social-area02">

                           <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
                           <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>
                           <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
                           </a>
                        </div>
                     </div>
                  </div>

               </div>
               <div class="search-area searchlinkClickHndler">
                  <img src="assets/images/search-icon.png" alt="" class="TopBarIconTp1 mrgRight50" />

               </div>
               <div class="searchform">
                  <form action="" class="row m-0">
                     <div class="SearchFormBox col">
                        <div class="row">
                           <div class="col-auto">
                              <span class="searchlinkClickHndler d-flex  align-items-center fullHeight">
                                 <span class="CloseIcnTp1">x</span>
                                 <span class="closeIconTextTp1">Close</span>
                              </span>
                           </div>
                           <div class="col">
                              <input type="text" class="SearchFormField" placeholder="Search our range of thousands of products..">
                           </div>
                           <div class="col-auto">
                              <button type="button" class="SearchBtn1">Search</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="LogoBoxTp1 DeskTopOption">
                  <div class="posRelative">
                     <a href="index.php" class="HeroLogo">
                        <img src="assets/images/logo.png" alt="Logo" />
                     </a>
                  </div>
               </div>
               <div class="d-flex  align-items-center fullHeight DeskTopOption">
                  <div class="NavLinkBox d-flex  align-items-center fullHeight">
                     <div class="NvLinkTp1 d-flex  align-items-center fullHeight">
                        <a href="products.php" class=" d-flex  align-items-center fullHeight justify-content-sm-center NvLinkTp1Item">Catalogue</a>
                        <div class="SubNavBox">
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">New Arrivals</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Seating</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Tables</a>

                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Bars & Food Stations</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="NvLinkTp1 d-flex  align-items-center fullHeight">
                        <a href="products.php" class=" d-flex  align-items-center fullHeight justify-content-sm-center NvLinkTp1Item">Collections</a>
                        <div class="SubNavBox">
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">New Arrivals</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Seating</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Tables</a>

                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Bars & Food Stations</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="NvLinkTp1 d-flex  align-items-center fullHeight">
                        <a href="gallery.php" class=" d-flex  align-items-center fullHeight justify-content-sm-center NvLinkTp1Item">Gallery</a>
                        <div class="SubNavBox">
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">New Arrivals</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomac</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Seating</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Tables</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">French cost</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Bars & Food Stations</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Decor</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Hospitality</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Flooring & Outdoors</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Acessories</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Miscellaneous</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="SubSystemBox">
                              <div class="SubItemTp1Box">
                                 <a href="products.php" class="SubNvLinkBoxLnk">Walls & Partitions</a>
                                 <div class="SubItemBoxTp1">
                                    <div class="flex-wrap SubItemBoxTp1Items d-flex  align-items-center fullHeight justify-content-sm-center">
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dreamscape</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Exotic Nomad</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Imperial</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Mexicola</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Dark & Moody</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Retro Remix</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink"> French Coast</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">White Bliss</a>
                                       <a href="products.php" class="SubItemBoxTp1ItemsLink">Luxe Style</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="NvLinkTp1 d-flex  align-items-center fullHeight">
                        <a href="services.php" class=" d-flex  align-items-center fullHeight justify-content-sm-center NvLinkTp1Item">Services</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="BxTpRight fullHeight align-items-center d-flex">
               <div class="tphoen DeskTopOption">800 MINT (6468)</div>
               <div class="PhoneIconBox">
                  <a href="https://api.whatsapp.com/send?phone=+971525995503" target="_blank">
                     <img src="assets/images/whatsapp.png" alt="" />
                  </a>
               </div>
               <div class="cartIconBox">
                  <a href="cart.php" class="CartIconTp1">
                     <img src="assets/images/truck-icon.png" alt="" />
                     <span>2</span>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="MobileHeader" id="HeaderMobile">
         <div>
            <div class="MobileHeaderContent">
               <div>
                  <div class="d-flex">
                     <div class="MobleMenuIcnTp1 MobleSideMenu">
                        <img class="mobileMenuIconTp1" src="assets/images/menu-icon.png" alt="Side Menu" />
                     </div>
                     <div class="MobileSideNav">
                        <div class="FixedBoxTp1Wrp">
                           <div class="FixedBoxTp1">
                              <div class="FixedBoxTp1Header">
                                 <div class="row align-items-center ">
                                    <div class="col">
                                       <div>
                                          <div class="FixedBoxTp1Txt1">Mint</div>
                                          <div class="FixedBoxTp1Txt2">Event Rentals</div>
                                       </div>
                                    </div>
                                    <div class="col-auto"><span class="CloseBtn CloseSideNav">x</span></div>
                                 </div>
                              </div>
                              <div class="FixedBoxTp1Content">
                                 <div class="ItemBoxTp1">
                                    <div class="ItemBoxTp1Hd1">All Categories</div>
                                    <div class="ItemBoxTp1Content">
                                       <div class="ItemBoxTp1LinkBox">
                                          <a href="products.php" class="ItemBoxTp1Lnk">Catalogues <span class="SubChildActived"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                                          <a href="products.php" class="ItemBoxTp1Lnk">Themes<span class="SubChildActived"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                                          <a href="products.php" class="ItemBoxTp1Lnk">Collection<span class="SubChildActived"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                                          <a href="gallery.php" class="ItemBoxTp1Lnk">Gallery<span class="SubChildActived"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                                       </div>

                                    </div>
                                 </div>
                                 <div class="ItemBoxTp1">
                                    <div class="ItemBoxTp1Hd1">Who we are ? </div>
                                    <div class="ItemBoxTp1Content">
                                       <div class="ItemBoxTp1LinkBox">
                                          <a href="about-us.php" class="ItemBoxTp1Lnk1">About Us</a>
                                          <a href="download-catalog.php" class="ItemBoxTp1Lnk1">Downloads</a>
                                          <a href="cart.php" class="ItemBoxTp1Lnk1">Cart</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="ItemBoxTp1">
                                    <div class="ItemBoxTp1Hd1">Quick Support </div>
                                    <div class="ItemBoxTp1Content">
                                       <div class="ItemBoxTp1LinkBox">
                                          <a href="faq.php" class="ItemBoxTp1Lnk2">FAQs</a>
                                          <a href="#" class="ItemBoxTp1Lnk2">Press release</a>
                                          <a href="#" class="ItemBoxTp1Lnk2">Blog</a>
                                          <a href="terms-and-conditions.php" class="ItemBoxTp1Lnk2">Terms & Conditions</a>
                                          <a href="contact-us.php" class="ItemBoxTp1Lnk2">Contact Us</a>
                                          <a href="#" class="ItemBoxTp1Lnk2">Testimonials</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="FixedBoxTp1Footer">
                                 <div class="social-area02">
                                    <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
                                    <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>
                                    <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
               <div>
                  <a href="index.php" class="MobileHeroLogo">
                     <img src="assets/images/logo.png" alt="Logo" />
                  </a>
               </div>
               <div>
                  <div class="d-flex justify-content-end">
                     <div class="MobleMenuIcnTp1 MobileSearchMenu MarginAdjustmentTp1">
                        <img class="mobileMenuIconTp1" src="assets/images/search-icon.png" alt="" class="TopBarIconTp1 mrgRight50" />
                     </div>
                     <div class="MobileSearchBox">
                        <div class="FixedBoxTp1Wrp">
                           <div class="FixedBoxTp1">
                              <div class="FixedBoxTp1Header">
                                 <div class="row align-items-center ">
                                    <div class="col">
                                       <div>
                                          <div class="FixedBoxTp1Txt1">Mint</div>
                                          <div class="FixedBoxTp1Txt2">Event Rentals</div>
                                       </div>
                                    </div>
                                    <div class="col-auto"><span class="CloseBtn CloseSearch">x</span></div>
                                 </div>
                              </div>
                              <div class="FixedBoxTp1Content">
                                 <div class="ItemBoxTp1">
                                    <div class="ItemBoxTp1Hd1">Search Our Products</div>
                                    <div class="ItemBoxTp1Content">
                                       <div class="BoxTpSearcBox">
                                          <div>
                                             <form action="" class="MobileSearchBoxField">
                                                <div>
                                                   <div>
                                                      <input type="text" placeholder="Search Our Products here..">
                                                   </div>
                                                   <div>
                                                      <button class="SearchSubmit"><img src="assets/images/search-icon.png" alt=""></button>
                                                   </div>
                                                </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="FixedBoxTp1Footer">
                                 <div class="social-area02">
                                    <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
                                    <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>
                                    <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="MobleMenuIcnTp1 "><a href="cart.php"><img class="mobileMenuIconTp2" src="assets/images/truck-icon.png" alt="" /></a></div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- <div class="top-area">
         <div class="logo" data-aos="fade-down" data-aos-duration="500"><a href="index.php"><img src="assets/images/logo.png" alt="Logo" /></a></div>
         <div class="container-fluid">
            <section class="row">
               <aside class=" col-sm-12 col-md-12 col-lg-3 col-xl-3">
                  <div class="sidemenu"><a href="#"><img src="assets/images/menu-icon.png" alt="Side Menu" /></a>
                     <div id="sideNavigation" class="sidenav">
                        <div class="sidenav-inner">
                           <a href="javascript::" class="closebtn">&times;</a> <a href="aboutus.php">
                              <h2>About us</h2>
                           </a>
                           <a href="catalogdownload.php" class="fancybox fancybox.iframe dl-btn">
                              <h2>Dowloads</h2>
                           </a>
                           <a href="cart.php">
                              <h2>Cart</h2>
                           </a>
                           <ul>
                              <li><a href="faq.php">FAQs</a></li>
                              <li><a href="#">press release</a></li>
                              <li><a href="#">blog</a></li>
                              <li><a href="termsandconditions.php">terms & Conditions</a></li>
                              <li><a href="contactus.php">Contact Us</a></li>
                              <li><a href="#">testimonials</a></li>
                           </ul>
                           <div class="clear"></div>
                           <div class="social-area02">

                              <a href="#" target="_blank"><i class="fa fb fa-facebook"></i></a>
                              <a href="#" target="_blank"><i class="fa twt fa-twitter"></i></a>
                              <a href="#" target="_blank"><i class="fa ig fa-instagram" aria-hidden="true"></i>
                              </a>
                           </div>
                        </div>
                     </div>

                  </div>
                  <div class="search-area" id="searchlink"><img src="assets/images/search-icon.png" alt="" />

                     <div class="searchform">
                        <form id="search" action="">
                           <input type="text" class="s" id="s" name="s" placeholder="keywords...">
                           <button type="submit" class="sbtn"><i class="fa fa-search"></i></button>
                        </form>
                     </div>



                  </div>
               </aside>
               <aside class=" col-sm-12 col-md-12 col-lg-5 col-xl-5">
                  <div class="navigation-area">
                     <nav class="navbar navbar-expand-lg navbar-light">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                           <span class="navbar-toggler-icon"><i class="fa fa-bars" aria-hidden="true"></i></span>
                           <div class="maintext">Main Menu</div>
                        </button>
                        <div class="mainmenu collapse navbar-collapse" id="navbarNavAltMarkup">
                           <ul id="nav">
                              <li class="has-children">
                                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-hover="dropdown">Catalogue</a>
                                 <ul class="dropdown-menu multi-level" role="menu">
                                    <li><a href="products.php">New Arrival</a></li>
                                    <li><a href="products.php">Seating</a></li>
                                    <li><a href="products.php">Tables</a></li>
                                    <li><a href="products.php">Bars & Food Stations</a></li>
                                    <li><a href="products.php">Decor</a></li>
                                    <li><a href="products.php">Hospitality</a></li>
                                    <li><a href="products.php">Flooring & Outdoor</a></li>
                                    <li><a href="products.php">Accessories</a></li>
                                    <li><a href="products.php">Miscellaneous</a></li>
                                    <li><a href="products.php">Walls & Partitions</a></li>
                                 </ul>
                              </li>
                              <li class="has-children">
                                 <a href="products.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Collections</a>
                                 <ul class="dropdown-menu multi-level" role="menu">
                                    <li><a href="products.php">Parties</a></li>
                                    <li class="has-children">
                                       <a href="products.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Weddings</a>
                                       <ul class="dropdown-menu multi-level" role="menu">
                                          <li><a href="products.php">Dreamscape</a></li>
                                          <li><a href="products.php">Exotic Nomad</a></li>
                                          <li><a href="products.php">Imperial</a></li>
                                          <li><a href="products.php">Mexicola</a></li>
                                          <li><a href="products.php">Dark & Moody</a></li>
                                          <li><a href="products.php">Retro Remix</a></li>
                                          <li><a href="products.php">French Coast</a></li>
                                          <li><a href="products.php">White Bliss</a></li>
                                          <li><a href="products.php">Luxe Style</a></li>
                                          <li><a href="products.php">Imperial</a></li>
                                          <li><a href="products.php">Retro Remix</a></li>
                                          <li><a href="products.php">Morrocan Sunset</a></li>
                                       </ul>
                                    </li>
                                    <li><a href="products.php">Coporate Events</a></li>
                                    <li><a href="products.php">Conferences</a></li>
                                    <li><a href="products.php">Exhibitions</a></li>
                                    <li><a href="products.php">Major Events</a></li>
                                 </ul>
                              </li>

                              <li class="has-children">
                                 <a href="gallery.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gallery</a>
                                 <ul class="dropdown-menu multi-level" role="menu">
                                    <li><a href="gallery.php">Parties</a></li>
                                    <li class="has-children">
                                       <a href="gallery.php" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Weddings</a>
                                       <ul class="dropdown-menu multi-level" role="menu">
                                          <li><a href="gallery.php">Dreamscape</a></li>
                                          <li><a href="gallery.php">Exotic Nomad</a></li>
                                          <li><a href="gallery.php">Imperial</a></li>
                                          <li><a href="gallery.php">Mexicola</a></li>
                                          <li><a href="gallery.php">Dark & Moody</a></li>
                                          <li><a href="gallery.php">Retro Remix</a></li>
                                          <li><a href="gallery.php">French Coast</a></li>
                                          <li><a href="gallery.php">White Bliss</a></li>
                                          <li><a href="gallery.php">Luxe Style</a></li>
                                          <li><a href="gallery.php">Imperial</a></li>
                                          <li><a href="gallery.php">Retro Remix</a></li>
                                          <li><a href="gallery.php">Morrocan Sunset</a></li>
                                       </ul>
                                    </li>
                                    <li><a href="gallery.php">Coporate Events</a></li>
                                    <li><a href="gallery.php">Conferences</a></li>
                                    <li><a href="gallery.php">Exhibitions</a></li>
                                    <li><a href="gallery.php">Major Events</a></li>
                                 </ul>

                              </li>
                           </ul>
                        </div>
                     </nav>
                  </div>
               </aside>
               <aside class=" col-sm-12 col-md-12 col-lg-4 col-xl-4">
                  <div class="topright">
                     <ul>
                        <li>
                           <div class="tphoen">800 MINT (6468)</div>
                        </li>
                        <li><img src="assets/images/phone-icon.png" alt="" /></li>
                        <li><img src="assets/images/truck-icon.png" alt="" /></li>
                     </ul>
                  </div>
               </aside>
            </section>
         </div>
      </div> -->

   </header>
   <!-- header finished here -->